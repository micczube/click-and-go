void EEPROMdefaults()
{
  //=============================== Funkcja zapisujaca w pamieci EEPROM standardowa konfiguracje ===============================
  byte defaults[] = 
  {60, 150, 150, 150, 150, 1, 0, 0,
  52, 188, 52, 188, 52, 188, 52, 188,
  3, 32, 3, 32, 3, 32, 3, 32,
  10, 10, 10, 10, 10, 10, 10, 10, 170};      // standardowa konfiguracja pamieci EEPROM

  for(int i=0; i<33; i++)
  {
    EEPROM.write(i, defaults[i]);
  }
  EEPROMload();
}

void EEPROMload()
{
  //=============================== Funkcja zapisujaca konfiguracje z pamieci EEPROM do zmiennych globalnych ===============================
  encoders = 1;
  lowbat = EEPROM.read(0);
  for(byte i=0;i<4;i++)
  {
    maxamps[i]=EEPROM.read(1+i);
    motrpm[i]=EEPROM.read(i*2+8)*256+EEPROM.read(i*2+9);
    encres[i]=EEPROM.read(i*2+16)*256+EEPROM.read(i*2+17);
    reserve[i]=EEPROM.read(24+i);
    stalltm[i]=long(EEPROM.read(28+i));
  }

  master = EEPROM.read(5);
  addroffset = EEPROM.read(6);
  i2cfreq = EEPROM.read(7);

  defaulted = EEPROM.read(32);
}

/*
    EEPROM MAP
    
    addr  description

    0     low bat volts:   60=6.0V
    1     M1 current:      255=2.55A
    2     M2 current:      255=2.55A
    3     M3 current:      255=2.55A
    4     M4 current:      255=2.55A

    5     master I²C addr: address of external master (default=1)
    6     I²C addr offset: shield address offset (default=0)
    7     I²C Clock:       0=100  1=400
    8     M1 RPM Hi:       Motor 1 RPM high byte
    9     M1 RPM Lo:       Motor 1 RPM low  byte
    10    M2 RPM Hi:       Motor 2 RPM high byte
    11    M2 RPM Lo:       Motor 2 RPM low  byte
    12    M3 RPM Hi:       Motor 3 RPM high byte
    13    M3 RPM Lo:       Motor 3 RPM low  byte
    14    M4 RPM Hi:       Motor 4 RPM high byte
    15    M4 RPM Lo:       Motor 4 RPM low  byte

    16    E1 Res Hi:       Encoder 1 resolution high byte
    17    E1 Res Lo:       Encoder 1 resolution low  byte
    18    E2 Res Hi:       Encoder 2 resolution high byte
    19    E2 Res Lo:       Encoder 2 resolution low  byte
    20    E3 Res Hi:       Encoder 3 resolution high byte
    21    E3 Res Lo:       Encoder 3 resolution low  byte
    22    E4 Res Hi:       Encoder 4 resolution high byte
    23    E4 Res Lo:       Encoder 4 resolution low  byte

    24    M1 Reserve:      Motor 1 reserve power
    25    M2 Reserve:      Motor 2 reserve power
    26    M3 Reserve:      Motor 3 reserve power
    27    M4 Reserve:      Motor 4 reserve power
    28    M1 stall time:   Motor 1 stall time
    29    M2 stall time:   Motor 2 stall time
    30    M3 stall time:   Motor 3 stall time
    31    M4 stall time:   Motor 4 stall time
        
    32    defaults flag    170=defaults loaded (170 = B10101010)
*/
