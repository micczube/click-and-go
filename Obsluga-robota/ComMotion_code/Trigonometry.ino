void Trigonometry()
{
  //=============================== Funkcja obliczajaca predkosc kazdego z silnikow ===============================
  if(mcu == 0)                                                                        // Kazdy z procesorow odpowiedzialny jest za dwa silniki
  {
    mspeed[0] = int(sin((angle + 120.0)*radconvert)*float(velocity)) + rotation;      // Predkosc silnika 1
    mspeed[1] = int(sin((angle)*radconvert)*float(velocity)) + rotation;              // Predkosc silnika 2
  }
  else
  {
    mspeed[2] = int(sin((angle - 120.0)*radconvert)*float(velocity)) + rotation;      // Predkosc silnika 3
    mspeed[3] = 0;                                                                    // Jeden z silnikow nieuzywany
  }
  return;
}
