﻿W poniższym pliku opisane są funkcje każdego z plików w obecnym folderze. Kod ten jest kodem działającym na motor shieldzie ComMotion odpowiedzialnym za obsługę silników, enkoderów oraz stan naładowania baterii. Motor shield wyposażony jest w dwa procesory ATmega328P, z których każdy musi być jednakowo zaprogramowany do poprawnego działania motor shielda. W kodzie zmienną rozróżniająca procesor jest zmienna "mcu", która sczytuje stan cyfrowego pina o numerze 16. Stan niski na tym pinie oznacza procesor pierwszy, stan wysoki procesor drugi. W kodzie znajdują się ify, które definiują który kawałek kodu wykona się dla danego procesora.
### ComMotion_code.ino
Główny plik obsługujący motor shielda to "ComMotion_code.ino". Jest to plik zawierający w sobie funkcje setup oraz loop wykonywne na procesorach oraz wszystkie zmienne globalne. W setupie umieszczone są między innymi polecenia włączające rezystory pull up niezbędne do komunikacji szeregowej, polecenia ustalające piny służące jako input oraz output jak i definicje obsługi przerwań. W funkcji loop sczytywane są wartości analogowe z pinów, obsługiwany jest przypadek niskiego poziomu baterii oraz aktualizowana jest kontrola silników.
### Commands.ino
Plik "Commands.ino" zawiera w sobie odpowiedzi na komendy uzyskane od mastera (w naszym przypadku Arduino) przez komunikację I^2C. Jeśli komenda dotyczy sterowania silników, to paczka danych musi zostać przekazana na drugi procesor, ponieważ Arduino komunikuje się w tej sprawie z procesorem pierwszym. W razie komend pytających o status silników, enkoderów lub baterii Arduino musi adresować każdy z procesorów oddzielnie, aby nie wprowadzać zbędnych zakłóceń i opóźnień w kontroli silników. W razie zapytania o jakiś stan każdy z procesorów odsyła masterowi odpowiednią paczkę danych.
### EEPROM.ino
Plik "EEPROM.ino" jest plikiem zawierającym standardową konfigurację zmiennych i zapisującym je do pamięci EEPROM urządzenia. Teoretycznie w podstawowym kodzie na motor shielda konfiguracja pamięci EEPROM mogła być zmieniana i aktualizowana, aczkolwiek zrezygnowałem z tej opcji ponieważ pierwotnie motor shield służy do obsługi wielu konfiguracji robota, a w naszym przypadku tylko jednej, dlatego też posiadana przez nas konfiguracja mogła zostać zhardkodowana w programie.
### I2C_Receive.ino
Plik "I2C_Receive.ino" zawiera funkcję obsługującą przerwanie dotyczące otrzymanie paczki danych przez I^2C. Wartość z magistrali sczytywana jest do globalnej tablicy "datapack", a jej rozmiar do globalnej zmiennej "packsize".
### I2C_Send.ino
Plik "I2C_Send.ino" zawiera funkcję obsługującą przerwanie dotyczące otrzymania prośby o paczkę danych.
### IOpins.h
Plik "IOpins.h" zawiera definicje używanych przez nas pinów analogowych i cyfrowych na motor shieldzie.
### Motors.ino
Plik "Motors.ino" zawiera funkcje obsługującą prędkość silników zależnie od stanu enkoderów i wysyła na piny PWM odpowiednie pulsacje. Zawiera on również obsługę przerwań enkoderów.
### PowerDown.ino
Plik "PowerDown.ino" zawiera obsługę niskiego stanu baterii. W razie niedoboru mocy wyłączane są wszystkie silniki na pierwszym procesorze oraz wysyłana jest komenda do drugiego procesora o wyzerowaniu wartości wszystkich zmiennych sterujących.
### Trigonometry.ino
Plik "Trigonometry.ino" zawiera funkcję wyliczającą poszczególne prędkości każdego z silników na podstawie trzech wartości otrzymanych od Arduino: velocity - prędkość wypadkowa robota, angle - kąt jazdy robota, rotation - prędkość obrotu robota.