void Commands()
{
  //=============================== Funkcja wykonujaca otrzymana od mastera komende ===============================
  if(mcu == 0 && command < 5)                             // Jesli jestesmy na MCU 1 i dostalismy komende 0-4 to przesylamy dane do MCU 2
  {
    Wire.beginTransmission(address+1);
    Wire.write(datapack,packsize);
    Wire.endTransmission();
  }

  if(command == 3 && packsize == 7)                       // Jesli jest to komenda trzecia i otrzymalismy 7 bajtow danych to obslugujemy silniki
  {
    velocity = datapack[1]*256 + datapack[2];             // Integery sa 16 bitowe dlatego dane w postaci int musza byc rozdzielane na
    angle = datapack[3]*256 + datapack[4];                // dwa bajty. Nastepnie kazda zmienna jest obliczana z sumy
    rotation = datapack[5]*256 + datapack[6];             // dwoch otrzymanych bajtow, przy czym bajt wazniejszy jest mnozony przez 256.

    if(velocity > 255) velocity = 255;                    // Jesli predkosc wieksza niz 255 to zostaje na maksymalnej wartosci
    if(velocity < -255) velocity = -255;                  // Tu to samo tylko ze ujemna

    while(angle >= 360)                                   // Jezeli kat wiekszy niz 359 to konwertujemy go do zakresu 0-359
    {
      angle -= 360;
    }
    while(angle < 0)                                      // Jezeli kat mniejszy niz 0 to konwertujemy go do zakresu 0-359
    {
      angle += 360;
    }

    if(rotation > 255) rotation = 255;                    // Jesli predkosc obrotu wieksza niz 255 to ustawiamy maksymalna
    if(rotation < -255) rotation = -255;                  // Predkosc obrotu w przeciwna strone

    Trigonometry();                                       // Wywolujemy funkcje obliczajaca predkosc kazdego z poszczegolnych silnikow
    command = 255;                                        // Wylaczamy komende
    return;
  }

  //=============================== Raport statusu ===============================
  if(command == 6 && packsize == 2)                       // Status każdy procesor raportuje oddzielnie, żeby nie interferować z obsługą silników
  {
    spsize = 0;
    int request = datapack[1];                            // Zmienna wskazujaca ktore dane mamy przeslac

    if(request&1) // Bit 0:                               // Zwracamy wartosci zliczen enkoderow
    {
      sendpack[spsize + 0] = highByte(acount);
      sendpack[spsize + 1] =  lowByte(acount);
      sendpack[spsize + 2] = highByte(bcount);
      sendpack[spsize + 3] =  lowByte(bcount);
      spsize += 4;                                        // Zwiekszamy rozmiar wysylanej paczki o rozmiar paczki tej prosby
    }

    if(request&2) // Bit 1:                               // Resetujemy zliczenia enkoderow
    {
      acount = 0;
      bcount = 0;
    }

    if(request&4) // Bit 2:                               // Zwracamy natezenia na silnikach
    {
      sendpack[spsize + 0] = highByte(analogvalue[0]);
      sendpack[spsize + 1] =  lowByte(analogvalue[0]);
      sendpack[spsize + 2] = highByte(analogvalue[1]);
      sendpack[spsize + 3] =  lowByte(analogvalue[1]);
      spsize += 4;                                        // Zwiekszamy rozmiar wysylanej paczki o rozmiar paczki tej prosby
    }

    if((request&8) && mcu == 0) // Bit 3 i MCU 1:         // Zwracamy wartosc napiecia baterii
    {
      sendpack[spsize + 0] = highByte(analogvalue[4]);
      sendpack[spsize + 1] =  lowByte(analogvalue[5]);
      spsize += 2;                                        // Zwiekszamy rozmiar wysylanej paczki o rozmiar paczki tej prosby
    }

    if(request&16) // Bit 4:                              // Zwracamy flage bledow
    {
      sendpack[spsize + 0] = eflag;
      spsize += 1;                                        // Zwiekszamy rozmiar wysylanej paczki o rozmiar paczki tej prosby
    }

    if(request&32) // Bit 5:                              // Zerujemy flage bledow
    {
      eflag = 0;
    }
    command = 255;                                        // Wylaczamy komende
  }
}
