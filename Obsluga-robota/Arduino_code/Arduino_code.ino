#include "Arduino.h"
#include <Wire.h>

//=============================== Zmienne motor shield ==============================================
int velocity=0, angle=0, rotation=0;                          // Zmienne obslugujace silniki; velocity - predkosc wypadkowa robota, angle - kat jazdy robota, rotation - predkosc obrotu robota
int ser_velocity=0, ser_rotation=0;                           // Zmienne sterujace z seriala
int zliczenia_enkoderow[4];                                   // Tablica przechowujaca zliczenia kazdego z enkoderow
String response = "";
String IMU_data = "IMU0/0/0/0/0/0/";
String encoders_data = "Enk0/0/0/0/";

//=============================== Zmienne IMU ==============================================
int SENSOR_SIGN[9] = {1,1,1,-1,-1,-1,1,1,1};                  //poprawne kierunki x,y,z - akcelerometr,zyroskop,magnetometr

// akcelerometr 8g czułości
// 3.9 mg/digit; 1 g = 256
#define GRAVITY 256                                           //To jest dokładnie 1g w odczycie z akcelerometru
#define STATUS_LED 13

float G_Dt=0.02;                                              // Czas całkowania czyli 50HZ

int AN[6];                                                    //lista do zapisywania wartosci akcelerometru i zyroskopu
int AN_OFFSET[6]={0,0,0,0,0,0};                               //Offset sensorow

int gyro_x;
int gyro_y;
int gyro_z;
int accel_x;
int accel_y;
int accel_z;

byte gyro_sat=0;

void setup() {
  Serial.begin(9600);
  
  //=============================== Setup komunikacji z motor shieldem ==============================================
  Wire.begin();                                               // Rozpoczynamy komunikacje I2C
  
  zeruj_enkodery();                                           // Zerujemy wartosci zliczen enkoderow

  for (int i = 0; i < 4; i++)
  {
    zliczenia_enkoderow[i] = 0;                               // Teraz tez musza miec zero
  }
  
  //=============================== Setup czujnika IMU ==============================================
  Accel_Init();
  Gyro_Init();

  delay(20);

  for(int i=0;i<32;i++)                                       // Bierzemy pierwsze 32 wartosci
  {
    Read_Accel();
    Read_Gyro();
    for(int y=0; y<6; y++){                                   // Dodajemy je i liczymy offset
      AN_OFFSET[y] += AN[y];
    }
  }

  for(int y=0; y<6; y++){                                     // Normalizujemy go
    AN_OFFSET[y] = AN_OFFSET[y]/32;
  }

  AN_OFFSET[5] -= GRAVITY*SENSOR_SIGN[5];                     // Uwzgledniamy grawitacje
  
  velocity = 0;
  angle = 0;
  rotation = 0;
  
  delay(1500);
}

void loop() {  
  if (Serial.available() > 0)
  {
    response = Serial.readStringUntil('\n');
    
    // Wysylanie danych na raspa
    Serial.print(encoders_data);
    Serial.print("$%$");
    Serial.println(IMU_data);
  }
  
  response.toUpperCase();
  if (find(response, "STEERING"))
  {
    int index1 = response.indexOf(":");
    int index2 = response.indexOf("/");
    int index3 = response.indexOf("*");
    int index4 = response.indexOf("&");
    String wartosc = "";
    for (int i = index1 + 1; i < index2; i++)
    {
      wartosc += response[i];
    }
    ser_velocity = wartosc.toInt();
    wartosc = "";
    for (int i = index2 + 1; i < index3; i++)
    {
      wartosc += response[i];
    }
    angle = wartosc.toInt();
    wartosc = "";
    for (int i = index3 + 1; i < index4; i++)
    {
      wartosc += response[i];
    }
    ser_rotation = wartosc.toInt();
  }
  if (find(response, "RESET"))
  {
    zeruj_enkodery();
  }

  //=============================== Tak na wszelki wypadek, zeby zmienne sterujace nie skakly o ogromne wartosci ==============================================
  if (ser_velocity == 0) velocity = 0;
  else if ((ser_velocity - velocity) > 50) velocity += 50;
  else if ((ser_velocity - velocity) < -50) velocity -= 50;
  else velocity = ser_velocity;

  if (ser_rotation == 0) rotation = 0;
  else if ((ser_rotation - rotation) > 50) rotation += 50;
  else if ((ser_rotation - rotation) < -50) rotation -= 50;
  else rotation = ser_rotation;
  
  //=============================== Upewniamy sie, ze zmienne kontrolujace silniki maja poprawne wartosci ==============================================
  if(velocity > 255) velocity = 255;
  if(velocity < -255) velocity = -255;

  while(angle > 359)
  {
    angle -= 360;
  }
  while(angle < 0)
  {
    angle += 360;
  }

  if(rotation > 255) rotation = 255;
  if(rotation < -255) rotation = -255;  

  //=============================== Wysylamy do motor shielda zmienne kontrolujace silniki ==============================================
  Wire.beginTransmission(30);
  Wire.write(3);                                                // Komenda nr 3 to obsluga silnikow
  Wire.write(highByte(velocity));
  Wire.write( lowByte(velocity));
  Wire.write(highByte(angle));
  Wire.write( lowByte(angle));
  Wire.write(highByte(rotation));
  Wire.write( lowByte(rotation));
  Wire.endTransmission();
  
  //=============================== Wysylamy do motor shielda zapytanie o liczbe zliczen enkoderow ==============================================
  Wire.beginTransmission(30);                                 // Wysylamy komende do MCU 1 na motor shieldzie
  Wire.write(6);                                              // Komenda 6 zwiazana jest ze zmianami statusu silnikow i enkoderow
  Wire.write(B00000001);                                      // Bit 0 - chcemy uzyskac liczbe zliczen enkoderow
  Wire.endTransmission();
    
  Wire.beginTransmission(31);                                 // Wysylamy komende do MCU 2 na motor shieldzie
  Wire.write(6);                                              // Komenda 6 zwiazana jest ze zmianami statusu silnikow i enkoderow
  Wire.write(B00000001);                                      // Bit 0 - chcemy uzyskac liczbe zliczen enkoderow
  Wire.endTransmission();
    
  if (Wire.requestFrom(30, 4) == 0)
  {
    ;
  }
  else
  {
    zliczenia_enkoderow[0] = Wire.read();
    zliczenia_enkoderow[0] <<= 8;
    zliczenia_enkoderow[0] |= Wire.read();
    zliczenia_enkoderow[1] = Wire.read();
    zliczenia_enkoderow[1] <<= 8;
    zliczenia_enkoderow[1] |= Wire.read();
  }
    
  if (Wire.requestFrom(31, 4) == 0)
  {
    ;
  }
  else
  {
    zliczenia_enkoderow[2] = Wire.read();
    zliczenia_enkoderow[2] <<= 8;
    zliczenia_enkoderow[2] |= Wire.read();
    zliczenia_enkoderow[3] = Wire.read();
    zliczenia_enkoderow[3] <<= 8;
    zliczenia_enkoderow[3] |= Wire.read();
  }
  encoders_data = "Enk";
  for(int i = 0; i < 4; i++)
  {
    encoders_data += String(zliczenia_enkoderow[i]);
    encoders_data += "/";
  }
  
  //=============================== Sczytujemy IMU i printujemy na serial (tymczasowo) ==============================================
  Read_Gyro();                                                // Czytamy zyroskop
  Read_Accel();                                               // CZytamy akcelerometr
  IMU_data = "IMU";
  IMU_data += String(gyro_x);
  IMU_data += "/";
  IMU_data += String(gyro_y);
  IMU_data += "/";
  IMU_data += String(gyro_z);
  IMU_data += "/";
  IMU_data += String(accel_x);
  IMU_data += "/";
  IMU_data += String(accel_y);
  IMU_data += "/";
  IMU_data += String(accel_z);
  IMU_data += "/";
}

void zeruj_enkodery()
{
  Wire.beginTransmission(30);                                 // Wysylamy cos do MCU 1 na motor shieldzie
  Wire.write(6);                                              // Komenda 6 zwiazana jest ze zmianami statusu silnikow i enkoderow
  Wire.write(B00100010);                                      // Bit 1 - zerujemy wartosci zliczen enkoderow; Bit 5 - zerujemy flage bledow
  Wire.endTransmission();

  Wire.beginTransmission(31);                                 // Wysylamy cos do MCU 2 na motor shieldzie
  Wire.write(6);                                              // Komenda 6 zwiazana jest ze zmianami statusu silnikow i enkoderow
  Wire.write(B00100010);                                      // Bit 1 - zerujemy wartosci zliczen enkoderow; Bit 5 - zerujemy flage bledow  
  Wire.endTransmission();  
}

// Funckja do znajdywania komend dla robota w przychodzacyh Stringach
boolean find(String string, String value){
  return string.indexOf(value)>=0;
}
