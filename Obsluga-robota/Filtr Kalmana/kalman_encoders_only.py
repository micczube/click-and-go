# Dzialajace na danych z pliku tekstowego same enkodery - przetestowane

"""
wektor stanu:
X = [
x
y
th
x*
y*
th*
x**
y**
]
gdzie th oznacza orientację robota, a * pochodną
"""

from pykalman import KalmanFilter
import numpy as np
import math

np.set_printoptions(linewidth=300)  # Do wyswietlania

n_dim_state = 8

dT = 0.02  # Krok czasowy

# Zmienne do enkoderow
# ====================================================
Ts = 7.267
enkodery = [0, 0, 0]
fi = [math.pi/6, math.pi - math.pi/6, math.pi + math.pi/2]
R_enc = 0.05
phi = 0
enkoder_1, enkoder_2, enkoder_3 = [], [], []

dT = Ts  # Na chwilę obecną okres uaktualniania enkoderów !!!
# ====================================================

# transition_matrix - przy jej użyciu zachodzi estymacja na podstawie poprzedniego stanu
A = [[1, 0, 0, dT,   0,  0, (dT**2)/2,         0],
     [0, 1, 0,  0,  dT,  0,         0, (dT**2)/2],
     [0, 0, 1,  0,   0, dT,         0,         0],
     [0, 0, 0,  1,   0,  0,        dT,         0],
     [0, 0, 0,  0,   1,  0,         0,        dT],
     [0, 0, 0,  0,   0,  0,         0,         0],
     [0, 0, 0,  0,   0,  0,         0,         0],
     [0, 0, 0,  0,   0,  0,         0,         0]]


# observation_matrix - określa które zmienne stanu są mierzone
# Same enkodery - x', y', th'
H = [[0, 0, 0, 1, 0, 0, 0, 0],
     [0, 0, 0, 0, 1, 0, 0, 0],
     [0, 0, 0, 0, 0, 1, 0, 0]]



# transition_covariance - określa błąd estymacji (jest dodawana przy każdej estymacji)
Q = [[0.8, 0, 0, 0, 0, 0, 0, 0],
     [0, 0.8, 0, 0, 0, 0, 0, 0],
     [0, 0, 0.8, 0, 0, 0, 0, 0],
     [0, 0, 0, 0.4, 0, 0, 0, 0],
     [0, 0, 0, 0, 0.4, 0, 0, 0],
     [0, 0, 0, 0, 0, 0.4, 0, 0],
     [0, 0, 0, 0, 0, 0, 0.4, 0],
     [0, 0, 0, 0, 0, 0, 0, 0.4]]


# initial_state_covariance - określa początkowy błąd estymacji
P0 = [[0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0]]


# initial_state_mean - wartosci poczatkowe wektora stanu
X0 = [0,
      0,
      0,
      0,
      0,
      0,
      0,
      0]

# observation_covariance - błąd pomiaru

# Same enkodery
R = [[0, 0, 0],
     [0, 0, 0],
     [0, 0, 0]]

# Stworzenie obiketu klasy KalmanFiltetr z zdefiniowanymi macierzami
kf = KalmanFilter(transition_matrices=A,
                  observation_matrices=H,
                  transition_covariance=Q,
                  observation_covariance=R,
                  initial_state_mean=X0,
                  initial_state_covariance=P0)

# Wektor stanu, aktualizowany w każdym kroku
filtered_state_means = np.zeros(n_dim_state)

# Macierz określająca błąd wyznaczonego stanu, aktualizowana w każdym kroku
filtered_state_covariances = np.zeros((n_dim_state, n_dim_state))

# Petla czasowa




# czytanie z pliku dopóki Wifi nie bedzie dzialalo
# ====================================================
with open("datas_encoders.txt", 'r') as file:
    for line in file:
        line = line.split(',')
        enkoder_1.append(line[0])
        enkoder_2.append(line[1])
        enkoder_3.append(line[3])
# ====================================================

# Petla czasowa, która stąd zniknie jak już wszystko połączymy na robocie
for i in range(len(enkoder_1) - 1):
    # try:
    # Przekształcenia na danych z enkoderow, by uzyskac predkosci robota
    # =====================================================================
    enkodery[0] = abs(float(enkoder_3[1]) - float(enkoder_3[0]))
    enkodery[1] = float(enkoder_2[1]) - float(enkoder_2[0])
    enkodery[2] = float(enkoder_3[1]) - float(enkoder_3[0])

    # Tu sa m/s
    enkodery[0] = ((enkodery[0])/(Ts * 8 * 78)) * 0.05 * math.pi
    enkodery[1] = ((enkodery[1])/(Ts * 8 * 78)) * 0.05 * math.pi
    enkodery[2] = ((enkodery[2])/(Ts * 8 * 78)) * 0.05 * math.pi

    # Model kinematyczny robota:
    v_x_loc = (2.0 / 3.0) * (
                -math.sin(fi[0]) * enkodery[0] - math.sin(fi[0] + fi[1]) * enkodery[1] - math.sin(fi[0] + fi[2]) *
                enkodery[2])
    v_y_loc = (2.0 / 3.0) * (
                math.cos(fi[0]) * enkodery[0] + math.cos(fi[0] + fi[1]) * enkodery[1] + math.cos(fi[0] + fi[2]) *
                enkodery[2])
    w = (2.0 / 3.0) * ((1.0 / (2.0 * R_enc)) * (enkodery[0] + enkodery[1] + enkodery[2]))
    # =====================================================================

    # Przeksztalcenie na układ początkowy robota (z jego obecnego, lokalnego ukladu)
    # Zakladamy, ze kat to wartosc obrotu przeciwnie do ruchu wskazowek zegara
    # -------------------------------------------------------------------------

    v_x = (v_x_loc * math.cos(filtered_state_means[2]) -
           v_y_loc * math.sin(filtered_state_means[2]))

    v_y = (v_x_loc * math.sin(filtered_state_means[2]) +
           v_y_loc * math.cos(filtered_state_means[2]))

    # -------------------------------------------------------------------------

    # Uaktualnianie macierzy kowariancji oraz wektora stanu (przewidywanie i pomiary)
    # observation - pomiary w tej chwili czasu
    filtered_state_means, filtered_state_covariances = kf.filter_update(
        filtered_state_means,
        filtered_state_covariances,
        observation=[v_x, v_y, w])

    # =====================================================================
    enkoder_1.pop(0)
    enkoder_2.pop(0)
    enkoder_3.pop(0)

    # =====================================================================
    # print(np.column_stack((['x', 'y', 'th', 'x*', 'y*', 'th*', 'x**', 'y**'], filtered_state_means)))
    # print("\n")









