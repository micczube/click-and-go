from threading import Thread
import time
import numpy as np
from numpy import transpose as tr


class KalmanFilterThread(Thread):
    def __init__(self, main_thread):
        Thread.__init__(self)
        self.MAIN_THREAD = main_thread
        self.is_running = False
        self.IMU = np.zeros(6)
        self.enkoders = np.zeros(3)
        self.robot_image_position = np.zeros(2)
        self.pos_est_is_started = False
        self.time_step = 0.005

        # Wektor stanu, aktualizowany w każdym kroku
        self.filtered_state_means = None

        # Macierz określająca błąd wyznaczonego stanu, aktualizowana w każdym kroku
        self.filtered_state_covariances = None

        # Macierze niezbedne do filtru Kalmana
        self.A = None
        self.H = None
        self.Q = None
        self.R = None

    def run(self):
        #----------------------

        """
        wektor stanu:
        X = [
        x
        y
        th
        x*
        y*
        th*
        x**
        y**
        ]
        """
        dT = 0.02  # Krok czasowy

        # transition_matrix - przy jej użyciu zachodzi estymacja na podstawie poprzedniego stanu
        self.A = [[1, 0, 0, dT,  0,  0, (dT**2)/2,         0],
                  [0, 1, 0,  0, dT,  0,         0, (dT**2)/2],
                  [0, 0, 1,  0,  0, dT,         0,         0],
                  [0, 0, 0,  1,  0,  0,        dT,         0],
                  [0, 0, 0,  0,  1,  0,         0,        dT],
                  [0, 0, 0,  0,  0,  0,         0,         0],
                  [0, 0, 0,  0,  0,  0,         0,         0],
                  [0, 0, 0,  0,  0,  0,         0,         0]]

        # observation_matrix - określa które zmienne stanu są mierzone
        # Enkodery (tylko x' i y') + IMU + obraz. W kolejnosci: x, y, x', y', th', x'', y''
        self.H = [[1, 0, 0, 0, 0, 0, 0, 0],
                  [0, 1, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 1, 0, 0, 0, 0],
                  [0, 0, 0, 0, 1, 0, 0, 0],
                  [0, 0, 0, 0, 0, 1, 0, 0],
                  [0, 0, 0, 0, 0, 0, 1, 0],
                  [0, 0, 0, 0, 0, 0, 0, 1]]

        # transition_covariance - określa błąd estymacji (jest dodawana przy każdej estymacji)
        self.Q = [[4.0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 4.0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0.5, 0, 0, 0, 0, 0],
                  [0, 0, 0, 3.0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 3.0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 4.0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 1.0, 0],
                  [0, 0, 0, 0, 0, 0, 0, 1.0]]


        # initial_state_covariance - określa początkowy błąd estymacji
        P0 = [[0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0]]

        # initial_state_mean - wartosci poczatkowe wektora stanu
        X0 = [0,
              0,
              0,
              0,
              0,
              0,
              0,
              0]

        # Enkodery + IMU + obraz(niezerowe wartości tylko dla x, y, x' i y' bo tylko one są też estymowane)
        self.R = [[0.5, 0,   0,   0,   0, 0, 0],
                  [0,   0.5, 0,   0,   0, 0, 0],
                  [0,   0,   0.5, 0,   0, 0, 0],
                  [0,   0,   0,   0.5, 0, 0, 0],
                  [0,   0,   0,   0,   0.01, 0, 0],
                  [0,   0,   0,   0,   0, 0.01, 0],
                  [0,   0,   0,   0,   0, 0, 0.01]]

        # Inicjalizacja
        self.filtered_state_means = np.copy(X0)
        self.filtered_state_covariances = np.copy(P0)
        # -----------------------------
        while self.is_running is True:
            pos_est_thread = Thread(target=self.position_estimation())
            pos_est_thread.start()
            time.sleep(self.time_step)

    def position_estimation(self):
        if self.pos_est_is_started is True:
            print("Wątek estymacji został zaczęty zanim poprzedni skończył działanie")
        self.pos_est_is_started = True
        # position = [0, 0, 0]  #               tutaj zapisac wynik estymacji!
        # ============================================
        # Faza predykcji
        # ------------------------------
        # Szacowanie stanu na podstawie poprzedniego
        x_pred = self.A @ self.filtered_state_means

        # Uaktualnieni macierzy kowariancji
        filtered_state_covariances = self.A @ self.filtered_state_covariances @ tr(self.A) + self.Q

        # Przewidywany pomiar
        z_pred = self.H @ x_pred
        # ------------------------------

        # Pomiar wlasciwy
        z = np.array([self.robot_image_position[0], self.robot_image_position[1], self.enkoders[0], self.enkoders[1],
                      self.IMU[0], self.IMU[1], self.IMU[2]])

        # Innowacja
        e = z - z_pred

        # Macierz kowariancji innowacji
        S = self.H @ self.filtered_state_covariances @ tr(self.H) + self.R

        # Wzmocnienie Kalmana
        K = self.filtered_state_covariances @ tr(self.H) @ np.linalg.inv(S)

        # Nowa estymata stanu
        self.filtered_state_means = x_pred + K @ e

        # Aktualizacja macierzy kowariancji
        self.filtered_state_covariances = (np.eye(K.shape[0], K.shape[0]) - K @ self.H) @ self.filtered_state_covariances

        position = [self.filtered_state_means[0], self.filtered_state_means[1]]  # x, y
        # ============================================

        send_pos = Thread(target=self.return_position(position))
        send_pos.start()
        self.pos_est_is_started = False

    def return_position(self, position):
        self.MAIN_THREAD.change_robot_kalman_position(position)
