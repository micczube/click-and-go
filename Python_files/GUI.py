import socket
import numpy as np
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import gui_files.server_gui as server_gui

default_img_path = 'images/android_temp.jpg'


# ==================== Główne okno GUI ====================
class Ui_MainWindow(QMainWindow):
    # ==================== Inicjalizacja ====================
    def __init__(self, main_thread):
        super(Ui_MainWindow, self).__init__()
        self.ui = server_gui.Ui_SerwerGUI()
        self.ui.setupUi(self)
        self.MAIN_THREAD = main_thread
        # Przyłączenie kontrolek ui do funkcji
        self.ui.cnn_button.clicked.connect(self.btn_cnn_click)
        self.ui.clear_button.clicked.connect(self.btn_clear_click)
        self.ui.robot_control_combo.currentIndexChanged.connect(self.robot_combo_changed)
        self.ui.ip_textbox.textChanged.connect(self.update_options_file)
        self.ui.port_textbox.textChanged.connect(self.update_options_file)
        self.ui.max_robot_vel_textbox.textChanged.connect(self.robot_vel_text_changed)
        self.ui.conn_opt_checkbox.stateChanged.connect(self.update_options_file)
        # Mozliwość wpisania tylko liczb w pole tekstowe z portem
        only_int_val = QIntValidator()
        self.ui.port_textbox.setValidator(only_int_val)
        self.ui.max_robot_vel_textbox.setValidator(only_int_val)
        # Zmienne wyświetlane na kontrolkach Qt
        self.started = False
        self.phone_connected = False
        self.robot_connected = False
        self.ip_textbox_text = '0.0.0.0'
        self.port_textbox_text = '5000'
        self.max_robot_vel_text = '30'
        self.info_textbox_text = []
        self.connection_btn_text = 'Start server'
        self.phone_sensor_text = ['0', '0', '0', '0', '0', '0']
        self.phone_click_text = ['0', '0']
        self.robot_sensor_text = ['0', '0', '0', '0', '0', '0']
        self.robot_enk_text = ['0', '0', '0']
        self.robot_position_text = ['0', '0', '0', '0']
        self.robot_control_combo_id = 0
        self.phone_image = QPixmap('images/android_temp.jpg')
        self.processed_image = QPixmap('images/android_temp.jpg')
        self.load_options_file()
        # Ustawienie początkowych wartości
        self.MAIN_THREAD.STATE.max_robot_velocity = int(self.ui.max_robot_vel_textbox.text())
        timer = QTimer()
        timer.singleShot(30, self.update_gui)

    # ==================== Funkcje zmiany informacji ====================
    # Funkcje wywoływane przez zewnętrzne wątki i zmieniające odpowiednie zmienne wewnętrzne GUI. Jest to wymagane,
    # ponieważ zewnętrzne wątki nie moga bezpośrednio zmieniać danych wyświetlanych na kontrolkach GUI.
    def append_info_textbox(self, message):
        self.info_textbox_text.append(message)

    def phone_connection_info(self, conn):
        self.phone_connected = conn

    def robot_connection_info(self, conn):
        self.robot_connected = conn

    def update_phone_sensor_text(self, sensor_data):
        for i in range(6):
            self.phone_sensor_text[i] = str(np.around(sensor_data[i], 3))

    def update_robot_IMU_text(self, sensor_data):
        for i in range(6):
            self.robot_sensor_text[i] = str(np.around(sensor_data[i], 3))

    def update_encoders(self, encoders_data):
        for i in range(3):
            self.robot_enk_text[i] = str(np.around(encoders_data[i], 3))

    def update_phone_click_text(self, click_data):
        for i in range(2):
            self.phone_click_text[i] = str(np.around(click_data[i], 3))

    def update_robot_vel_text(self, robot_vel, robot_rot):
        pass

    def update_robot_positon(self, current, next):
        for i in range(2):
            self.robot_position_text[i] = str(np.around(current[i], 3))
            self.robot_position_text[i + 2] = str(np.around(next[i], 3))

    def update_image(self, img):
        height, width, channel = img.shape
        bytesPerLine = 3 * width
        self.phone_image = QImage(img.data, width, height, bytesPerLine, QImage.Format_RGB888).rgbSwapped()

    def update_processed_image(self, img):
        height, width, channel = img.shape
        bytesPerLine = 3 * width
        self.processed_image = QImage(img.data, width, height, bytesPerLine, QImage.Format_RGB888).rgbSwapped()

    # ==================== Funkcje kontrolek GUI ====================
    def btn_clear_click(self):
        # Funkcja czyszcząca box z informacjami
        self.info_textbox_text = []

    def btn_cnn_click(self):
        # Funckcja nawiązująca połączenie z serwerem
        if self.started is False:
            # Sprawdzenie czy serwer nie jest już odpalony
            conn_valid = True
            server_temp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            server_temp.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            # Sprawdzanie, czy na podanym IP i porcie można otworzyć serwer.
            # Sprawdzana jest poprawność IP dla komputera oraz poprawność i dostępność portu.
            try:
                socket.inet_aton(self.ui.ip_textbox.text())
                try:
                    int(self.ui.port_textbox.text())
                    if 0 <= int(self.ui.port_textbox.text()) <= 65535:
                        try:
                            server_temp.bind((self.ui.ip_textbox.text(), int(self.ui.port_textbox.text())))
                            conn_result = server_temp.connect_ex(
                                (self.ui.ip_textbox.text(), int(self.ui.port_textbox.text())))
                            # if conn_result != 0:
                            #     info = "Port {} is already reserved.".format(self.port_textbox.text())
                            #     self.append_info_textbox(info)
                            #     conn_valid = False
                        except socket.error:
                            info = "<span style=\" font-size:8pt; font-weight:600; color:#ff0000;\" >"
                            info += "IP address {} is not valid for this device.".format(self.ui.ip_textbox.text())
                            info += "</span>"
                            self.append_info_textbox(info)
                            conn_valid = False
                    else:
                        info = "<span style=\" font-size:8pt; font-weight:600; color:#ff0000;\" >"
                        info += "Port {} is invalid(Out of range).".format(self.ui.port_textbox.text())
                        info += "</span>"
                        self.append_info_textbox(info)
                        conn_valid = False
                except ValueError:
                    info = "<span style=\" font-size:8pt; font-weight:600; color:#ff0000;\" >"
                    info += "Port {} is not an integer.".format(self.ui.port_textbox.text())
                    info += "</span>"
                    self.append_info_textbox(info)
                    conn_valid = False
            except socket.error:
                info = "<span style=\" font-size:8pt; font-weight:600; color:#ff0000;\" >"
                info += "IP address {} is invalid.".format(self.ui.ip_textbox.text())
                info += "</span>"
                self.append_info_textbox(info)
                conn_valid = False

            if conn_valid is True:
                self.MAIN_THREAD.start_server(self.ui.ip_textbox.text(), int(self.ui.port_textbox.text()))
                # Zmiana napisu na guziku ze start na stop
                self.connection_btn_text = "Stop Server"
                self.started = True
                # Aktualizacja tablicy powiadomień
        else:
            self.MAIN_THREAD.stop_server()
            self.connection_btn_text = "Start Server"
            self.started = False

    def robot_combo_changed(self):
        self.ui.robot_control_combo_id = self.ui.robot_control_combo.currentIndex()
        if self.ui.robot_control_combo.currentIndex() == 0:
            self.MAIN_THREAD.set_robot_control_mode('click')
        elif self.ui.robot_control_combo.currentIndex() == 1:
            self.MAIN_THREAD.set_robot_control_mode('manual')

    def robot_vel_text_changed(self):
        try:
            robot_vel = int(self.ui.max_robot_vel_textbox.text())
            if robot_vel < 1:
                self.ui.max_robot_vel_textbox.setText('1')
                robot_vel = 1
            elif robot_vel > 255:
                self.ui.max_robot_vel_textbox.setText('255')
                robot_vel = 255
            self.MAIN_THREAD.STATE.max_robot_velocity = robot_vel
            self.update_options_file()
        except ValueError:
            self.ui.max_robot_vel_textbox.setPlaceholderText(self.max_robot_vel_text)

    # ==================== Funkcje obsługi plików ====================
    def update_options_file(self):
        self.ip_textbox_text = self.ui.ip_textbox.text()
        self.port_textbox_text = self.ui.port_textbox.text()
        self.max_robot_vel_text = self.ui.max_robot_vel_textbox.text()
        filename = 'temp/options.txt'
        try:
            f = open(filename, 'w+')
            f.write(self.ui.ip_textbox.text() + '\n')
            f.write(self.ui.port_textbox.text() + '\n')
            f.write(self.ui.max_robot_vel_textbox.text() + '\n')
            if self.ui.conn_opt_checkbox.isChecked():
                f.write('conn_msgs 1\n')
            else:
                f.write('conn_msgs 0\n')
            f.close()
        except IOError:
            pass

    def load_options_file(self):
        try:
            filename = 'temp/options.txt'
            f = open(filename, 'r')
            lines = f.readlines()
            ip_text = lines[0].strip()
            self.ui.ip_textbox.setText(ip_text)
            port = int(lines[1].strip())
            self.ui.port_textbox.setText(str(port))
            robot_vel = int(lines[2].strip())
            self.ui.max_robot_vel_textbox.setText(str(robot_vel))
            opt_str = 'conn_msgs'
            if lines[3].find(opt_str) != -1:
                id = lines[3].find(opt_str)
                opt = int(lines[3][(id + len(opt_str)):].strip())
                if opt == 0:
                    self.ui.conn_opt_checkbox.setChecked(False)
                elif opt == 1:
                    self.ui.conn_opt_checkbox.setChecked(True)
            f.close()
        except (IOError, ValueError, IndexError):
            pass

    # ==================== Funkcja odświeżania GUI ====================
    def update_gui(self):
        # Funkcja włączana co określony czas przez QTimer(), która odświeża zawartość elementów GUI przy użyciu danych
        # zawartych w zmiennych wewnętrznych. Jest to konieczne, ponieważ z innych wątków nie można bezpośrenio
        # zmieniać danych wyświetlanych na kontroklach GUI.

        # info textbox
        self.ui.info_textbox.setText('')
        for msg in self.info_textbox_text:
            self.ui.info_textbox.append(msg)
        # połączenie z telefonem
        if self.phone_connected is True:
            self.ui.phone_status_textbox.setStyleSheet("color: rgb(0, 255, 0);")
            status = "Connected"
            self.ui.phone_status_textbox.setText(status)
        else:
            self.ui.phone_status_textbox.setStyleSheet("color: rgb(255, 0, 0);")
            status = "Disconnected"
            self.ui.phone_status_textbox.setText(status)
        # połączenie z robotem
        if self.robot_connected is True:
            self.ui.robot_status_textbox.setStyleSheet("color: rgb(0, 255, 0);")
            status = "Connected"
            self.ui.robot_status_textbox.setText(status)
        else:
            self.ui.robot_status_textbox.setStyleSheet("color: rgb(255, 0, 0);")
            status = "Disconnected"
            self.ui.robot_status_textbox.setText(status)
        # sensory telefonu
        self.ui.xa_phone.setText(self.phone_sensor_text[0])
        self.ui.ya_phone.setText(self.phone_sensor_text[1])
        self.ui.za_phone.setText(self.phone_sensor_text[2])
        self.ui.xg_phone.setText(self.phone_sensor_text[3])
        self.ui.yg_phone.setText(self.phone_sensor_text[4])
        self.ui.zg_phone.setText(self.phone_sensor_text[5])
        # IMU robota
        self.ui.xa_robot.setText(self.robot_sensor_text[0])
        self.ui.ya_robot.setText(self.robot_sensor_text[1])
        self.ui.za_robot.setText(self.robot_sensor_text[2])
        self.ui.xg_robot.setText(self.robot_sensor_text[3])
        self.ui.yg_robot.setText(self.robot_sensor_text[4])
        self.ui.zg_robot.setText(self.robot_sensor_text[5])
        # robot enkodery
        self.ui.xenc.setText(self.robot_enk_text[0])
        self.ui.yenc.setText(self.robot_enk_text[1])
        self.ui.zenc.setText(self.robot_enk_text[2])
        # kliknięcie na ekran
        self.ui.x_click.setText(self.phone_click_text[0])
        self.ui.y_click.setText(self.phone_click_text[1])
        # pozycja robota
        self.ui.x_currpos.setText(self.robot_position_text[0])
        self.ui.ycurrpos.setText(self.robot_position_text[1])
        self.ui.xnextpos.setText(self.robot_position_text[2])
        self.ui.ynextpos.setText(self.robot_position_text[3])
        # przycisk serwera
        self.ui.cnn_button.setText(self.connection_btn_text)
        # obraz z kamery
        scene = QGraphicsScene()
        item = QGraphicsPixmapItem()
        item.setPixmap(QPixmap(self.phone_image).scaled(self.ui.phone_img_view.width() - 10,
                                                        self.ui.phone_img_view.height(), Qt.KeepAspectRatio))
        scene.addItem(item)
        self.ui.phone_img_view.setScene(scene)
        # obraz z telefonu
        scene = QGraphicsScene()
        item = QGraphicsPixmapItem()
        item.setPixmap(QPixmap(self.processed_image).scaled(self.ui.processed_img_view.width() - 10,
                                                            self.ui.processed_img_view.height(), Qt.KeepAspectRatio))
        scene.addItem(item)
        self.ui.processed_img_view.setScene(scene)
        # ustawianie timera funkcji updatującej
        timer = QTimer()
        timer.singleShot(30, self.update_gui)
