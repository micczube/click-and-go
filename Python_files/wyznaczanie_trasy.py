from math import ceil
import networkx as nx
from wyznaczanie_trasy_graf import construct_graph, find_path
import cv2


def get_path(r, frame, rx, ry, clicked_coor):
    """
    Wyznaczanie trasy do wskazanego punktu
    :param r: Promień, którym wyraża się kontur robota
    :param frame: Aktualna klatka na filmie
    :param rx: Współrzędna x położenia robota (środek) - robot_pos_pix[0]
    :param ry: Współrzędna y położenia robota (środek) - robot_pos_pix[1]
    :param clicked_coor: Zmapowane współrzędne kliknięcia na ekran (z funkcji Marcina)
    :return:
        path: Lista węzłów, która tworzy trasę od aktualnego położenia robota do punktu docelowego
        frame: Klatka, na której wyrysowana zostaje trasa do punktu docelowego
    """
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)  # wymagane w konstruowaniu grafu i wyznaczaniu trasy
    map_size_x, map_size_y = 600, 400  # Rozmiar układu współrzędnych
    cell_size_x, cell_size_y = 20, 20  # Rozmiar pojedynczej komorki
    # Rozmiar grafu
    graph_size_x = int(map_size_x / cell_size_x)
    graph_size_y = int(map_size_y / cell_size_y)
    # Margines - zapas bezpieczeństwa aby robot nie uderzył w przeszkodę
    margin = ceil(2 * r / cell_size_x)
    # Graf wlasciwy
    G = nx.Graph()
    construct_graph(G, graph_size_x, graph_size_y, frame, cell_size_x, cell_size_y, margin)
    robot_pos = int(rx / cell_size_x), int(ry / cell_size_y)
    # end_node jest przekazywany poprzez klikniecie na ekranie
    end_node = (clicked_coor[0] // cell_size_x, clicked_coor[1] // cell_size_y)

    try:
        # trasa jest wyznaczana w oparciu o algorytm A*
        path = find_path(G, robot_pos, end_node)

        # Rysowanie trasy
        prev_point = (rx, ry)
        for node in path:
            end_point = (node[0] * cell_size_x, node[1] * cell_size_y)
            frame = cv2.line(frame, prev_point, end_point, (0, 0, 255), 4)
            prev_point = end_point
        frame = cv2.cvtColor(frame, cv2.COLOR_GRAY2BGR)  # Zmiana na kolor

        return path, frame, True
    # Zabezpieczenie przed błędami wynikającymi z próby wyznaczenia trasy do niedostępnego węzła
    except (nx.exception.NodeNotFound, nx.exception.NetworkXNoPath):
        print("Dojazd do wskazanego punktu niemożliwy")
        return [], None, False





