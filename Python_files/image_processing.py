import os
import functools
import time
from threading import Thread
import cv2
import numpy as np
from determine_initial_robot_orientation import determine_initial_orientation, transform_coor
from wyznaczanie_trasy import get_path
from utilities import find_objects, calibrate, transform_click
from math import radians, sin, cos

default_img_path = 'images/android_temp.jpg'


class ImgThread(Thread):
    def __init__(self, main_thread):
        Thread.__init__(self)
        self.MAIN_THREAD = main_thread
        self.image = cv2.imread(default_img_path)
        self.is_running = False
        self.calibration_request = False
        self.color_calibration_complete = False
        self.robot_calibration_complete = False
        self.route_mapping_mode = False
        self.robot_found = False
        self.robot_calibration_steps_remaining = 0
        self.robot_kalman_position = np.zeros(2)
        self.click = [-1, -1]
        self.mapped_click = [-1, -1]
        self.x_start = 0
        self.y_start = 0
        self.angle = 0
        
        self.out_test = cv2.VideoWriter("calibration.avi", cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 30, (600, 240)) # Moze (320, 400) zamiast (600, 400)
        self.frame_to_file = None
        self.saved_img = False
        self.save_img_counter = 0 # Po zrobieniu kalibracji zaczyna sie zwikeszanie countera i gdy przekroczy on wartosc (np. 50) to filmik jest zapisywany
        
    def run(self):
        # Deklaracja zmiennych do wyznaczenia początkowego ruchu
        p_x, p_y = [], []  # wspolrzedne przechowywane do wyznaczenia poczatkowej orientacji robota

        # Progi kolorów (do tworzenia HSV)
        orange_h, orange_s, orange_v_min = 22, 70, 100
        robot_l_h, robot_l_s, robot_l_v_min = 13, -50, 20
        robot_h_h, robot_h_s, robot_h_v_min = 20, 50, 179

        # inicjalizacja zakresów do wycinania HSV (pudelka maja dwa zakresy, bo może być na początku i końcu skali)
        orange_lo, orange_hi, orange_lo2, orange_hi2 = None, None, None, None
        robot_lo, robot_hi = None, None
        # inicjalizacja starej klatki, macierzy perspektywy, wielkosci kalibratora
        old_frame, matrix, area = None, None, 0

        while self.is_running is True:
            if self.calibration_request is True:
                print("POCZĄTEK KALIBRACJI")
                cv2.waitKey(1000)  # aby przypadkiem nie kalibrować na obrazku android_temp.jpg

                # czy kalibracja się powiodła, rgb poprawkowe, macierz przekształceń oraz wielkość kalibratora
                self.color_calibration_complete, r0, g0, b0, matrix, area = calibrate(self.image)
                orange_rgb = [240, 90, 29]  # kolor pudelek (do wyznaczania zakresów HSV na podstawie kalibracji)
                robot_rgb = [88, 82, 156]  # kolor robota

                # korekcja idealnego pomarańczowego rgb na podstawie rgb korekcyjnego, potem zamiana na HSV
                orange_rgb[0] = np.min([255, orange_rgb[0] + r0])
                orange_rgb[1] = np.min([255, orange_rgb[1] + g0])
                orange_rgb[2] = np.min([255, orange_rgb[2] + b0])
                orange = np.uint8([[[orange_rgb[0], orange_rgb[1], orange_rgb[2]]]])
                orange_hsv = cv2.cvtColor(orange, cv2.COLOR_RGB2HSV)
                h0, s0, v0 = orange_hsv[0, 0, 0], orange_hsv[0, 0, 1], orange_hsv[0, 0, 2]

                # tworzenie zakresów HSV do wycinania na podstawie wyznaczonych wyżej h0, s0, v0
                # jeśli dolna granica HSV pomarańczowego jest ujemna, to trzeba wykorzystać drugi koniec skali
                orange_lo = np.array([max(0, h0 - orange_h), max(0, s0 - orange_s), orange_v_min], np.uint8)
                orange_hi = np.array([min(179, h0 + orange_h), 255, 255], np.uint8)

                orange_lo = np.array([20, 10, 210], np.uint8)  # zahardkodowane pudelka
                orange_hi = np.array([160, 255, 255], np.uint8)

                robot_rgb[0] = np.min([255, robot_rgb[0] + r0])  # analogicznie dla fioletu
                robot_rgb[1] = np.min([255, robot_rgb[1] + g0])
                robot_rgb[2] = np.min([255, robot_rgb[2] + b0])
                robot_color = np.uint8([[[robot_rgb[0], robot_rgb[1], robot_rgb[2]]]])
                robot_hsv = cv2.cvtColor(robot_color, cv2.COLOR_RGB2HSV)
                h0, s0, v0 = robot_hsv[0, 0, 0], robot_hsv[0, 0, 1], robot_hsv[0, 0, 2]
                robot_lo = np.array([max(0, h0 - robot_l_h), max(0, s0 + robot_l_s), robot_l_v_min], np.uint8)
                robot_hi = np.array([min(179, h0 + robot_h_h), min(255, s0 + robot_h_s), robot_h_v_min], np.uint8)

                robot_lo = np.array([50, 10, 100], np.uint8)  # zahardkodowany robot CZERWONY
                robot_hi = np.array([175, 70, 190], np.uint8)

                # jeśli kalibracja się nie udała to poczekaj chwilę i spróbuj ponownie
                if not self.color_calibration_complete:
                    print("BŁĄD KALIBRACJI! Skalibruj ponownie.")
                    self.MAIN_THREAD.EVENTS.calibration_in_process = False
                    self.MAIN_THREAD.add_phone_command("Calibration error")
                    info = "<span style=\" font-size:8pt; font-weight:600; color:#000000;\" >"
                    info += "Błąd kalibracji koloru! Spróbuj ponownie."
                    info += "</span>"
                    self.MAIN_THREAD.add_new_message(info)
                    cv2.waitKey(1000)
                else:
                    print("Kalibracja poprawna. Dane kalibracji:")
                    print("RGB korekcyjne: {}, {}, {}".format(r0, g0, b0))
                    print("Pole kalibratora: {}".format(area))
                    print("Zakresy pomarańczowego:")
                    print(orange_lo, orange_hi)
                    print(orange_lo2, orange_hi2)
                    print("Zakres fioletowego:")
                    print(robot_lo, robot_hi)
                    self.MAIN_THREAD.EVENTS.calibration_in_process = True
                    self.MAIN_THREAD.add_phone_command("calibrated")
                    self.robot_calibration_steps_remaining = 100
                    info = "<span style=\" font-size:8pt; font-weight:600; color:#ff00ff;\" >"
                    info += "Rozpoczęto kalibrację robota..."
                    info += "</span>"
                    self.MAIN_THREAD.add_new_message(info)
                    cv2.waitKey(1)

                self.calibration_request = False

            if self.color_calibration_complete:
                # zwraca zmapowaną klatkę z obiektami
                frame_detected, robot_pos_pix, radius, self.robot_found = find_objects(self.image, matrix, orange_lo,
                                                                                       orange_hi, robot_lo, robot_hi,
                                                                                       area)

                if not self.robot_found:
                    self.MAIN_THREAD.add_phone_command('robot')
                # poniższy kod zmniejsza migotanie obrazu scalając obecną klatkę z poprzednią
                if old_frame is not None:
                    frame_2D = cv2.bitwise_and(frame_detected, old_frame)
                else:
                    frame_2D = frame_detected
                # self.MAIN_THREAD.set_gui_frame2d(cv2.resize(frame_2D, (320, 240)))  # wyswietlana jest później klatka z narysowaną wyznaczoną trasą
                old_frame = frame_detected
                # cv2.imshow('Mobile camera', frame_2D)
                cv2.waitKey(1)

                # jeśli ktoś kliknie na ekran
                point_status = True
                if self.click[0] != -1 and self.click[1] != -1:
                    # mapowanie kliknięcia i wyzerowanie żądania kliknięcia
                    self.mapped_click = transform_click(self.click[0], self.click[1], self.image, matrix)
                    self.click = [-1, -1]
                    point_status = False

                # ====================
                # Blok instrukcji do wyznaczenia poczatkowego ruchu
                if self.robot_calibration_complete is False:
                    # estymacja poczatkowej orientacji robota ma miejsce w ostatnim kroku kalibracji
                    if self.robot_calibration_steps_remaining == 1:
                        self.robot_calibration_steps_remaining = 0
                        self.robot_calibration_complete = True
                        p_x.append(robot_pos_pix[0])
                        p_y.append(robot_pos_pix[1])
                        # Wyznaczenie polozenia i kata ukladu poczatkowego
                        # punkt startowy to punkt w ktorym robot zakonczyl wyznaczanie kata poczatkowego
                        self.x_start, self.y_start, self.angle = determine_initial_orientation(p_x, p_y)
                        self.angle = radians(self.angle)
                        print("Wyznaczono poczatkową orientację.")
                    if self.robot_calibration_steps_remaining <= 0:
                        self.robot_calibration_steps_remaining = 0
                        # Wspolrzedne w poczatkowym ukladzie
                        robot_pos_init_coor = transform_coor(self.x_start, self.y_start, self.angle, robot_pos_pix)
                        
                        # Rysowanie poczatkowego ukladu wspolrzednych
                        # ==========================================================================================================
                        self.frame_to_file = cv2.circle(frame_2D, (self.x_start, self.y_start), 5, color=(255, 0, 0), thickness=-1)
                        # os X (uklad poczatkowy)
                        self.frame_to_file = cv2.line(self.frame_to_file, (int(self.x_start), int(self.y_start)), (int(self.x_start + 300*cos(self.angle)), int(self.y_start-300*sin(self.angle))), color=(0, 255, 0), thickness=2)
                        # os Y (uklad poczatkowy)
                        self.frame_to_file = cv2.line(self.frame_to_file, (int(self.x_start), int(self.y_start)), (int(self.x_start - 300*sin(self.angle)), int(self.y_start-300*cos(self.angle))), color=(0, 255, 0), thickness=2)

                        # polozenie X (w ukladzie poczatkowym)
                        # x_start = x_start (poczatek) +
                        # robot_pos_init_coor[0]*cos(angle) (keirunek osi OX) + (kierunek prostopadly do X, czyli Y lub -Y)
                        self.frame_to_file = cv2.line(self.frame_to_file, (int(self.x_start + robot_pos_init_coor[0]*cos(self.angle) - 300*sin(self.angle)), int(self.y_start-robot_pos_init_coor[0]*sin(self.angle) - 300*cos(self.angle))),
                                         (int(self.x_start + robot_pos_init_coor[0]*cos(self.angle)+300*sin(self.angle)), int(self.y_start-robot_pos_init_coor[0]*sin(self.angle)+300*cos(self.angle))),
                                         color=(0, 0, 255), thickness=2)
                        # polozenie Y (w ukladzie poczatkowym)
                        self.frame_to_file = cv2.line(self.frame_to_file, (int(self.x_start-robot_pos_init_coor[1]*sin(self.angle) + 300*cos(self.angle)), int(self.y_start-robot_pos_init_coor[1]*cos(self.angle) - 300*sin(self.angle))),
                                         (int(self.x_start-robot_pos_init_coor[1]*sin(self.angle) - 300*cos(self.angle)), int(self.y_start-robot_pos_init_coor[1]*cos(self.angle)+300*sin(self.angle))), color=(0, 0, 255), thickness=2)
                        # print(robot_pos_pix)
                        # print(robot_pos_init_coor)
                        self.out_test.write(self.frame_to_file)
                        # ==========================================================================================================
                        
                    else:
                        p_x.append(robot_pos_pix[0])
                        p_y.append(robot_pos_pix[1])
                        self.robot_calibration_steps_remaining -= 1

                elif self.robot_calibration_complete is True and self.route_mapping_mode is True:
                    # WYZNACZANIE TRASY
                    # -------------------------------
                    # Przekształcenie położenia robota na nowy układ współrzędnych
                    # (wykorzystywane aktualnie do pracy z filtrem Kalmana)
                    coor_pos = transform_coor(self.x_start, self.y_start, self.angle, robot_pos_pix)
                    self.MAIN_THREAD.change_robot_img_position([coor_pos[0], coor_pos[1]])

                    path, frame_with_route, found = get_path(radius, frame_2D, robot_pos_pix[0], robot_pos_pix[1],
                                                             self.mapped_click)
                                                             
                    # Rysowanie poczatkowego ukladu wspolrzednych 2
                    # ==========================================================================================================
                    if self.save_img_counter < 100:
                        self.save_img_counter += 1
                        self.frame_to_file = cv2.circle(frame_2D, (self.x_start, self.y_start), 5, color=(255, 0, 0), thickness=-1)
                        # os X (uklad poczatkowy)
                        self.frame_to_file = cv2.line(self.frame_to_file, (int(self.x_start), int(self.y_start)), (int(self.x_start + 300*cos(self.angle)), int(self.y_start-300*sin(self.angle))), color=(0, 255, 0), thickness=2)
                        # os Y (uklad poczatkowy)
                        self.frame_to_file = cv2.line(self.frame_to_file, (int(self.x_start), int(self.y_start)), (int(self.x_start - 300*sin(self.angle)), int(self.y_start-300*cos(self.angle))), color=(0, 255, 0), thickness=2)


                        self.frame_to_file = cv2.line(self.frame_to_file, (int(self.x_start + coor_pos[0]*cos(self.angle) - 300*sin(self.angle)), int(self.y_start-coor_pos[0]*sin(self.angle) - 300*cos(self.angle))),
                                         (int(self.x_start + coor_pos[0]*cos(self.angle)+300*sin(self.angle)), int(self.y_start-coor_pos[0]*sin(self.angle)+300*cos(self.angle))),
                                         color=(0, 0, 255), thickness=2)
                        # polozenie Y (w ukladzie poczatkowym)
                        self.frame_to_file = cv2.line(self.frame_to_file, (int(self.x_start-coor_pos[1]*sin(self.angle) + 300*cos(self.angle)), int(self.y_start-coor_pos[1]*cos(self.angle) - 300*sin(self.angle))),
                                         (int(self.x_start-coor_pos[1]*sin(self.angle) - 300*cos(self.angle)), int(self.y_start-coor_pos[1]*cos(self.angle)+300*sin(self.angle))), color=(0, 0, 255), thickness=2)
                        self.out_test.write(self.frame_to_file)
                        print(f"in calibrated coordination system: x = {coor_pos[0]}, y = {coor_pos[1]}")
                    else:
                        self.out_test.release() # Zakonczenie zapisywania filmu
                        self.out_test.write(self.frame_to_file)
                        print(f"in calibrated coordination system: x = {coor_pos[0]}, y = {coor_pos[1]}")

                    if not self.saved_img:
                        self.saved_img = True
                        cv2.imwrite("images/calibration.jpg", self.frame_to_file)
                    # ==========================================================================================================
                    
                    if found:
                        # wyswietlenie klatki z wyznaczona trasa w GUI
                        self.MAIN_THREAD.set_gui_frame2d(cv2.resize(frame_with_route, (320, 240)))

                        # STEROWANIE ROBOTEM
                        # -------------------------------
                        if len(path) >= 2:
                            transformed_robot_coor = transform_coor(self.x_start, self.y_start, self.angle,
                                                                    robot_pos_pix)
                            print(f"path: {path}")
                            print(f"robot_pos_pix: {robot_pos_pix} ==> {transformed_robot_coor}")

                            # Współrzędne węzła z trasy są zamieniane na współrzędne na obrazie a następnie
                            # przekształcane do nowego układu współrzędnych
                            next_node_pix_x = (path[1][0] * 20) + 10
                            next_node_pix_y = (path[1][1] * 20) + 10
                            next_node_pix = (next_node_pix_x, next_node_pix_y)
                            transformed_next_node_pix = transform_coor(self.x_start, self.y_start, self.angle,
                                                                       next_node_pix)
                            print(f"next_node_pix: {next_node_pix} ==> {transformed_next_node_pix}")

                            # Zmienne do wyznaczenia kąta
                            xs, ys = transformed_robot_coor[0], transformed_robot_coor[1]
                            xe, ye = transformed_next_node_pix[0], transformed_next_node_pix[1]
                            dy = ye - ys
                            dx = xe - xs

                            # predkosc oraz kat kierunku jazdy wysylana do robota
                            robot_vel = self.MAIN_THREAD.STATE.max_robot_velocity
                            robot_ang = int(0 - np.degrees(np.arctan2(dy, dx)) % 360.0)  # przeciwnie do ruchu wskazowek zegara
                            # self.MAIN_THREAD.set_robot_gui_position(robot_pos_pix, next_node_pix)
                        else:
                            robot_vel = 0
                            robot_ang = 0
                            self.route_mapping_mode = False
                            info = "<span style=\" font-size:8pt; font-weight:600; color:#ff00ff;\" >"
                            info += "Robot dojechał do celu!"
                            info += "</span>"
                            self.MAIN_THREAD.add_phone_command("finished")
                            self.MAIN_THREAD.add_new_message(info)

                        print(f"robot_vel: {robot_vel}, robot_ang: {robot_ang} \n")
                        robot_rot = 0
                        self.MAIN_THREAD.change_robot_control_variables([robot_vel, robot_ang, robot_rot])
                        self.MAIN_THREAD.set_gui_robot_vel(robot_vel, robot_ang)
                    else:
                        self.MAIN_THREAD.set_gui_frame2d(cv2.resize(frame_2D, (320, 240)))
                        if not point_status:
                            self.MAIN_THREAD.add_phone_command("point")

            time.sleep(0.02)