from threading import Thread
import time
import numpy as np
from pykalman import KalmanFilter
import math


class KalmanFilterThread(Thread):
    def __init__(self, main_thread):
        Thread.__init__(self)
        self.MAIN_THREAD = main_thread
        self.is_running = False
        self.IMU = np.zeros(6)
        self.enkoders = np.zeros(3)
        self.robot_image_position = np.zeros(2)
        self.pos_est_is_started = False
        self.time_step = 0.005

        # Wektor stanu, aktualizowany w każdym kroku
        self.filtered_state_means = None

        # Macierz określająca błąd wyznaczonego stanu, aktualizowana w każdym kroku
        self.filtered_state_covariances = None

        # Obiekt klasy KalmanFilter
        self.kf = None

    def run(self):
        #----------------------

        """
        wektor stanu:
        X = [
        x
        y
        th
        x*
        y*
        th*
        x**
        y**
        ]
        """
        dT = 0.02  # Krok czasowy

        # transition_matrix - przy jej użyciu zachodzi estymacja na podstawie poprzedniego stanu
        A = [[1, 0, 0, dT,  0,  0, (dT**2)/2,         0],
             [0, 1, 0,  0, dT,  0,         0, (dT**2)/2],
             [0, 0, 1,  0,  0, dT,         0,         0],
             [0, 0, 0,  1,  0,  0,        dT,         0],
             [0, 0, 0,  0,  1,  0,         0,        dT],
             [0, 0, 0,  0,  0,  0,         0,         0],
             [0, 0, 0,  0,  0,  0,         0,         0],
             [0, 0, 0,  0,  0,  0,         0,         0]]

        # observation_matrix - określa które zmienne stanu są mierzone
        # Enkodery (tylko x' i y') + IMU + obraz. W kolejnosci: x, y, x', y', th', x'', y''
        H = [[1, 0, 0, 0, 0, 0, 0, 0],
             [0, 1, 0, 0, 0, 0, 0, 0],
             [0, 0, 0, 1, 0, 0, 0, 0],
             [0, 0, 0, 0, 1, 0, 0, 0],
             [0, 0, 0, 0, 0, 1, 0, 0],
             [0, 0, 0, 0, 0, 0, 1, 0],
             [0, 0, 0, 0, 0, 0, 0, 1]]

        # transition_covariance - określa błąd estymacji (jest dodawana przy każdej estymacji)
        Q = [[0.8, 0, 0, 0, 0, 0, 0, 0],
             [0, 0.8, 0, 0, 0, 0, 0, 0],
             [0, 0, 0.8, 0, 0, 0, 0, 0],
             [0, 0, 0, 0.8, 0, 0, 0, 0],
             [0, 0, 0, 0, 0.8, 0, 0, 0],
             [0, 0, 0, 0, 0, 3.8, 0, 0],
             [0, 0, 0, 0, 0, 0, 3.8, 0],
             [0, 0, 0, 0, 0, 0, 0, 3.8]]

        # initial_state_covariance - określa początkowy błąd estymacji
        P0 = [[0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0]]

        # initial_state_mean - wartosci poczatkowe wektora stanu
        X0 = [0,
              0,
              0,
              0,
              0,
              0,
              0,
              0]

        # Enkodery + IMU + obraz(niezerowe wartości tylko dla x, y, x' i y' bo tylko one są też estymowane)
        R = [[0.7, 0,   0,   0,   0, 0, 0],
             [0,   0.7, 0,   0,   0, 0, 0],
             [0,   0,   0.7, 0,   0, 0, 0],
             [0,   0,   0,   0.7, 0, 0, 0],
             [0,   0,   0,   0,   0, 0, 0],
             [0,   0,   0,   0,   0, 0, 0],
             [0,   0,   0,   0,   0, 0, 0]]

        # Stworzenie obiektu klasy KalmanFiltetr ze zdefiniowanymi macierzami
        self.kf = KalmanFilter(transition_matrices=A,
                               observation_matrices=H,
                               transition_covariance=Q,
                               observation_covariance=R,
                               initial_state_mean=X0,
                               initial_state_covariance=P0)

        # Inicjalizacja
        self.filtered_state_means = np.copy(X0)
        self.filtered_state_covariances = np.copy(P0)
        #-----------------------------
        while self.is_running is True:
            pos_est_thread = Thread(target=self.position_estimation())
            pos_est_thread.start()
            time.sleep(self.time_step)

    def position_estimation(self):
        if self.pos_est_is_started is True:
            print("Wątek estymacji został zaczęty zanim poprzedni skończył działanie")
        self.pos_est_is_started = True
        # position = [0, 0, 0]  #               tutaj zapisac wynik estymacji!
        # -----------------------------------

        self.filtered_state_means, self.filtered_state_covariances = self.kf.filter_update(
            self.filtered_state_means,
            self.filtered_state_covariances,
            observation=[self.robot_image_position[0], self.robot_image_position[1], self.enkoders[0], self.enkoders[1],
                         self.IMU[0], self.IMU[1], self.IMU[2]])
        # 'x', 'y', 'th', 'x*', 'y*', 'th*', 'x**', 'y**'
            
        position = [self.filtered_state_means[0], self.filtered_state_means[1]]  # x, y
        # position = [self.filtered_state_means[0], self.filtered_state_means[1], self.filtered_state_means[2]] # x,y,th

        # -----------------------------

        send_pos = Thread(target=self.return_position(position))
        send_pos.start()
        self.pos_est_is_started = False

    def return_position(self, position):
        self.MAIN_THREAD.change_robot_kalman_position(position)
