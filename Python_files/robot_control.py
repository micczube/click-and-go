from threading import Thread
import threading
from pynput import keyboard
import time


# ==================== Wątek obsługi sterowania robotem ====================
class RobotControlThread(Thread):
    # Wątek, który obsługuje sterowanie robotem. Decyduje on, jaką postać będą mieć wiadomości wysyłane na robota.
    # Sterowanie może odbywać się przy użyciu klawiatury, lub przez zmianę zmiennych wewnętrznych RobotControlThread
    # z innych wątków(sterowanie automatyczne)

    # ==================== Inicjalizacja ====================
    def __init__(self, main_thread):
        Thread.__init__(self)
        self.MAIN_THREAD = main_thread
        self.new_steering_event = threading.Event()
        self.listener = keyboard.Listener(
            on_press=self.on_press,
            on_release=self.on_release)
        self.is_running = False
        self.dir_v = 0
        self.dir_h = 0
        self.dir_r = 0
        self.reset = False
        self.stop = False
        self.curr_robot_vel = 0
        self.curr_robot_angle = 0
        self.curr_robot_rot = 0

    # ==================== Funkcja run ====================
    def run(self):
        # Funkcja przekazuje wiadomość, jaka ma zostać wysłana na robota, kiedy dostanie informację o zmianie
        # zmiennych sterujących.
        self.listener.start()  # rozpoczynanie działania keyboard listenera
        while self.is_running is True:
            # Oczekiwanie na nową komendę sterującą robotem
            self.new_steering_event.wait()
            robot_dir_msg = ''
            # Zmiana zmiennych sterujących w przypadku sterowania z klawiatury
            if self.dir_h != 0:
                self.curr_robot_vel += self.dir_h * 10
                self.dir_h = 0
            if self.dir_v != 0:
                self.curr_robot_angle += self.dir_v * 10
                self.dir_v = 0
            if self.dir_r != 0:
                self.curr_robot_rot += self.dir_r * 10
                self.dir_r = 0

            if self.reset is True:
                robot_dir_msg += '$%$RESET$%$'
                self.curr_robot_vel = 0
                self.curr_robot_angle = 0
                self.curr_robot_rot = 0
                self.reset = False
            if self.stop is True:
                robot_dir_msg += '$%$STOP$%$'
                self.curr_robot_vel = 0
                self.curr_robot_angle = 0
                self.curr_robot_rot = 0
                self.stop = False
            # Obsługa przekroczenia przez zmienne maksymalnych wartości określonych przez fizycnze możliwości robota
            if self.curr_robot_vel > 255:
                self.curr_robot_vel = 255
            elif self.curr_robot_vel < -255:
                self.curr_robot_vel = -255
            while self.curr_robot_angle > 359:
                self.curr_robot_angle -= 360
            while self.curr_robot_angle < 0:
                self.curr_robot_angle += 360
            if self.curr_robot_rot > 255:
                self.curr_robot_rot = 255
            elif self.curr_robot_rot < -255:
                self.curr_robot_rot = -255
            # Stworzenie i przekazanie wiadomości, która ma zostać wysłana na robota
            robot_dir_msg += '$%$' + 'VEL' + str(self.curr_robot_vel) + '$%$'
            robot_dir_msg += 'ANGLE' + str(self.curr_robot_angle) + '$%$'
            robot_dir_msg += 'ROT' + str(self.curr_robot_rot) + '$%$'
            self.MAIN_THREAD.set_robot_command(robot_dir_msg)
            self.new_steering_event.clear()  # Resetowanie flagi

    # ==================== Dodatkowe funkcje ====================
    def change_control_variables(self, control_array):
        # Funkcja wywoływana z zewnętrznych wątków w celu przekazania zmiennych sterujących(sterowanie automatyczne)
        self.dir_v = 0
        self.dir_h = 0
        self.dir_r = 0
        self.curr_robot_vel = control_array[0]
        self.curr_robot_angle = control_array[1]
        self.curr_robot_rot = control_array[2]
        self.new_steering_event.set()

    # ==================== Keyboard listener ====================
    # Keyboard listener odpowiedzialny za zmianę zmiennych sterujących w przypadku sterowania z klawiatury
    def on_press(self, key):
        key_char = ''
        try:
            key_char = key.char
        except AttributeError:
            pass
        if self.MAIN_THREAD.STATE.robot_control_mode == 'manual':
            if key_char == 'w':
                self.dir_h = 1
                self.new_steering_event.set()
            elif key_char == 's':
                self.dir_h = -1
                self.new_steering_event.set()
            elif key_char == 'd':
                self.dir_v = 1
                self.new_steering_event.set()
            elif key_char == 'a':
                self.dir_v = -1
                self.new_steering_event.set()
            elif key_char == 'q':
                self.dir_r = -1
                self.new_steering_event.set()
            elif key_char == 'e':
                self.dir_r = 1
                self.new_steering_event.set()
            elif key_char == 'r':
                self.reset = True
                self.new_steering_event.set()
            elif key_char == 'g':
                self.stop = True
                self.new_steering_event.set()

    def on_release(self, key):
        key_char = ''
        try:
            key_char = key.char
        except AttributeError:
            pass
        if self.MAIN_THREAD.STATE.robot_control_mode == 'manual':
            if key_char == 'w' and self.dir_h == 1:
                self.dir_h = 0
            elif key_char == 's' and self.dir_h == -1:
                self.dir_h = 0
            elif key_char == 'd' and self.dir_v == 1:
                self.dir_v = 0
            elif key_char == 'a' and self.dir_v == -1:
                self.dir_v = 0
            elif key_char == 'e' and self.dir_r == 1:
                self.dir_r = 0
            elif key_char == 'q' and self.dir_r == -1:
                self.dir_r = 0
