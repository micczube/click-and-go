Sprawozdanie z tworzenia instalator�w pod system Windows i Linux. 

1) System Windows

Do stworzenia instalatora dla systemu Windows u�yto programu InnoSetup. Po uruchomieniu instalatora ClickAndGo installer nale�y post�powa� zgodnie
z intukcj� wy�wietlaj�c� si� na ekranie, mo�na wybra� opcje stworzenia skr�tu na pulpicie.  Instalator zosta� stworzony na podstawie pliku executable stworzonego przy
pomocy pyinstaller. 

2) System Linux

Przy tworzeniu instalatora na system linux pojawi�o si� klika problem�w:

- Nie mog�am u siebie na komputerze dokona� instalacji systemu operacyjnego ubuntu, z tego wzgl�du, �e mia� on konflikt 
z rozwi�zaniem Intel RST, ktore zarz�dza dyskami twardymi. Probowa�am trzech r�nych wersji ubuntu 18.04.4 , 19.10 oraz 20.04, ale �adna
z nich nie znajdowa�a wolnej partycji dysku, kt�r� wydzieli�am �eby zainstalowa� ubuntu. Rozwi�zaniem tego problemu mog�oby
by� wy��czenie RST w BIOS-ie komputera. Poniewa� na tym samym komputerze mam zainstalowany system
Windows (a jest to na t� chwile jedyny komputer w domu), kt�ry korzysta z rozwi�zanie RST, Windows m�g�by
si� przesta� uruchamia�, poniewa�  nie m�g�y znale�� dysk�w twardych. Ponadto mog�oby to prowadzi�
do nieodrwacalnej utraty danych co jest opisane na stronie ubuntu:
https://discourse.ubuntu.com/t/ubuntu-installation-on-computers-with-intel-r-rst-enabled/15347

Po wprowadzniu niezb�dnych zmian aby umo�liwi� ubuntu instalacj� obok systemu windows, mog�aby wyst�pi�
sytuacja w kt�rej system windows by sie nie uruchamia�, a w tym wypadku by�oby konieczne odyskanie
systemu windows. 

Poniewa� taka sytuacja mog�aby mi uniemo�liwi� prac� na komputerze przez najbli�sze dni,
co jest mi w tym okresie bardzo potrzebne, Pawe� �ogin zasugerowa�, �e na ubuntu u siebie sprawdzi
poprawno�� dzia�ania instalatora, kt�ry napisz� dla Linuxa.


Folder z instalatorem dla linuxa zawiera 3 pliki:

- Instalator dla systemu Linux x86
- Instalator dla systemu Linux x86 64 (Stworzone przy pomocy programu Installjammer)
- Zip, w kt�rym znajduj� si� wszystkie potrzbne pliki do instalacji, kt�re powy�sze dwa
instalatory wypakowuj� do foleru.

Po rozpakowaniu zipa (lub gdy zrobi to instalator) w folderze znaduj� si� pliki �r�d�owe aplikacji,
napisany przeze mnie instalator bash oraz plik README. Bash instalator, pobiera i instaluje pythona3 oraz wszystkie potrzebne biblioteki
do aplikacji, a nast�pnie j� uruchamia. Na komputerze Paw�a wyst�pi� problem z pobraniem
modu�u pyqt5 przez basha, natomiast przez wpisanie r�czne: sudo apt-get install python3-pyqt5 , ju� ten problem
si� nie pojawia�, dlatego w razie tak wyst�puj�cego problemu, zosta�a napisana instukcja README, kt�ra krok po kroku
t�umaczy, jak uruchomi� aplikacj� gdy takowy problem si� pojawi.




