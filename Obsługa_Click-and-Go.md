# Instrukcja obsługi Click-and-Go

## 1) Konfiguracja serwera
Klikamy na ikonkę serwera. Po uruchomieniu w pole IP wpisujemy IP naszego komputera. Portem domyślnym jest port 5000, ale to możemy zmienić zależnie od naszych potrzeb. Następnie naciskamy przycisk "Start serwer". W okienku serwera istnieją dwa pola tekstowe, na których w aktualnej chwili jest napisane "Disconnected". Kiedy połączymy się z aplikacją i robotem na polach tych będzie napis "Connected". W polu tekstowym po lewej stronie pokazują się najnowsze komunikaty na temat połączeń i kalibracji wysyłanych na serwer. W każdym momencie możemy zatrzymać serwer przyciskiem "stop serwer". Robot przestanie się wtedy poruszać. 


## 2) Robot
### a) Konfiguracja rapsberry  PI
Żeby skonfigurować rapsberry potrzebny nam będzie zewnętrzny monitor (to nie może być laptop), kabel HDMI, myszka i klawiatura. Podpinamy płytkę pod monitor używając kabla HDMI. Następnie podłączamy rapsberry PI do powerbanka. Kolejność jest tu bardzo ważna, bo inaczej płytka może się włączyć w innym trybie i nie przesyłać niczego na port HDMI. Następnie do wolnych portów płytki podpinamy myszkę i klawiaturę ( arduino musi być podłączone pod urządzeniem wifi na płytce, bo ten konkretny slot jest  wykorzystywany w programie pythona do komunikacji z Arduino).  Kiedy rapsberry się uruchomi wchodzimy w konsolę i tam musimy skonfigurować połączenie Wi-Fi z routerem, do którego podłączony jest server (najczęściej nasza lokalna sieć), następującym ciągiem komend: 

1. Wpisujemy "sudo su".
2. wpa_passphrase network_ssid network_pass (terminal wyświetli nam passphrase dla naszej sieci w postaci network = { coś coś coś }, kopiujemy wszystko do schowka) 
3.  cd Desktop
4.  nano wpa_supplicant.conf. Po otwarciu pliku usuwamy całą jego zawartość i wklejamy to co skopiowaliśmy przed chwila. Zapisujemy i wychodzimy.
5.  wpa_supplicant -B -i wlan0 -c wpa_supplicant.conf (powinno sie pojawic wpa_supplicant initialization succesfull) 
6.  watch -n 3 ifconfig (tutaj co 3 sekundy będzie nam sie odświeżał config interfejsów sieciowych, robimy to żeby sprawdzić czy interfejs wlan0 się ustawił, jeśli pojawi sie ipv4 zaczynające się "192.168..." to jest ok, zapamiętujemy IP) 

Teraz już możemy odłączyć płytkę od wszystkiego poza powerbankiem, przykleić go do robota i podłączyć arduino - wchodzimy w aplikację Putty na Windowsie albo w konsole na Linuxie. Łączymy się z adresem IP naszego rapsberry przez SSH - jako login podajemy "pi" a jako hasło "ksemwetipg". Jesteśmy zalogowani na płytkę przez ssh i możemy sobie kontrolować normalnie jej konsole: 
 1. cd /home/pi/Desktop 
 2. nano TCP_robot_client.py (tutaj wchodzimy w klienta robota w pythonie i zmieniamy tylko i wyłącznie ip servera, zapisujemy i wychodzimy) 
 3. python TCP_robot_client.py (klient powinien połączyć się z serwerem i wszystko jest ok)
 
### b) Włączenie motor shielda
Motor shield jest zasilany koszyczkiem baterii ze spodu robota. Wkładamy biało-czerwony kabelek do wolnego slota w motor shieldzie (obok czarnego kabelka) i przykręcamy. Na Arduino nie trzeba nic robić, ponieważ jest ono połączone kablem z rapsberry i jest zasilane z jego powerbanka.


## 3) Aplikacja android
Ściągamy najnowszą wersję aplikacji na telefon z Gitlaba projektu i ją instalujemy. Uruchamiamy aplikację kliknięciem na jej ikonkę. Po uruchomieniu pokazuje nam się nam ekran startowy, w którym możemy wpisać IP serwera i port (patrz punkt pierwszy). Nasze ustawienia zatwierdzamy naciśnięciem wielkiego zielonego guzika. Następnie pokazuje nam się okno aparatu z przyciskami "Połącz", "Kalibruj" i ikonka klucza. Po naciśnięciu w ikonkę klucza jesteśmy w stanie zmienić ustawienia aplikacji (IP, port). Po naciśnięciu przycisku "Połącz" zostaniemy połączeni z serwerem, a po naciśnięciu przycisku "Kalibruj" nastąpi kalibracja obrazu i robot będzie gotowy do pracy.

## 4) Dalsze kroki
1) Na serwerze klikamy "Start server"
2) W aplikacji Putty (bądź konsoli na Linuksie) łączymy się z Rapsberry. Komendą cd Desktop przechodzimy do pulpitu.
3) Wpisujemy komendę "nano TCP_robot_client.py" i zmieniamy tylko i wyłącznie ip servera (oraz port jeżeli używamy innego niż domyślny).
4) Następnie wpisujemy python TCP_robot_client.py (klient powinien polaczyc sie z serwerem, a ikonka "Disconnected" przy napisie Robot powinna się zamienić na "Connected")
5) Przechodzimy do aplikacji android na telefonie. Po ustawieniu konfiguracji naciskamy przycisk połącz (aplikacja powinna połączyć się z serwerem, a ikonka "Disconnected" przy napisie Mobile powinna się zamienić na "Connected").
6) Ustawiamy naszego robota w miejscu, w którym chcemy nim sterować. W prawym dolnym rogu układamy białą kartkę A4. Ustawiamy telefon tak żeby kartka była faktycznie w prawym dolnym rogu aparatu i naciskamy przycisk "Kalibruj". Jeżeli kalibracja powiedzie się dostaniemy stosowny komunikat, jeżeli się nie powiedzie także. W przypadku braku powodzenia należy spróbować ponownie (zwrócić uwagę na stabilność telefonu). Po udanej kalibracji nastąpi wyznaczanie orientacji początkowej. Robot poruszy się i wyznaczy swoją orientację. Ważne jest aby w tej fazie trzymać telefon jak najbardziej stabilnie. 
7) Kiedy warunki z punktu 6 zostaną spełnione można przejść do użytkowania robota. Możemy w dowolnym punkcie ekranu nacisnąć palcem i jeżeli nie będzie to przeszkoda robot dojedzie we wskazane miejsce. Kiedy robot dojedzie w to miejsce zatrzyma się, a my możemy śmiało ruszać naszym telefonem i podążyć za robotem. 
Następnie możemy wskazać kolejny punkt bądź rozłączyć się.
