package com.example.testcamera;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import androidx.fragment.app.DialogFragment;


public class CreateDialog extends DialogFragment  {

    //FUNKCJA DO ODEBRANIA ZMIENNEJ
    public static CreateDialog which_one(int w) {
        CreateDialog fragment = new CreateDialog();

        Bundle bundle = new Bundle();
        bundle.putInt("which_one", w);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // PRZYGOTOWANIE OKNA, WYGLADU
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View promptView = inflater.inflate(R.layout.dialog_layout, null);
        builder.setView(promptView);

        //ODEBRANIE ZMIENNEJ BLEDU
        int w = getArguments().getInt("which_one");
        TextView text = (TextView)promptView.findViewById(R.id.text);
        Button button =(Button)promptView.findViewById(R.id.button);

        // KOMUNIKAT ZALEZNY OD RODZAJU BLEDU
        switch (w) {
            case 0:
                text.setText(R.string.robot);
                button.setText(R.string.ok);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    dismiss();
                    }
                });
                break;

            case 1:
                text.setText(R.string.cal_e);
                button.setText(R.string.ok);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                    }
                });
                break;

            case 2:
                text.setText(R.string.point);
                button.setVisibility(View.GONE);
                break;

            case 3:
                text.setText(R.string.wrong_port);
                button.setText(R.string.ok);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                    }
                });
                break;

            case 4:
                text.setText(R.string.calibration);
                button.setText(R.string.ok);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                    }
                });
                break;

            case 5:
                text.setText(R.string.finished);
                button.setText(R.string.ok);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                    }
                });
                break;
        }

       return builder.create();
    }

}