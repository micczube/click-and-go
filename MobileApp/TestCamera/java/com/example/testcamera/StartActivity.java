package com.example.testcamera;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
    }

    public void SwitchActivity(View view) {

        EditText ip=(EditText)findViewById(R.id.IP);
        EditText port=(EditText)findViewById(R.id.Port);

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra ( "ip", ip.getText().toString() );
        intent.putExtra ( "port", port.getText().toString() );
        startActivity(intent);

        finish();
    }
}
