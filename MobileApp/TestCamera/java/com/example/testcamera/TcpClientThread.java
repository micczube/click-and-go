package com.example.testcamera;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageButton;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Queue;


public class TcpClientThread implements Runnable {
    private String SERVER_IP;
    private int SERVER_PORT;
    private Context mContext;
    private MainActivity mActivityInstance;
    private boolean mRun = true;
    private Socket socket_client;
    private int error_count;
    private boolean socket_is_sending = false;
    private BufferedReader in_msg_socket;
    private Queue<String> request_queue = new LinkedList<>();


    public TcpClientThread(Context context, String server_ip, int server_port) {
        super();
        mContext = context;
        SERVER_IP = server_ip;
        SERVER_PORT = server_port;
        mActivityInstance = (MainActivity)mContext;
        error_count = 0;
    }


    public void run() {
        //  Główna pętla wątku Socketa klienta. Początkowo łączy się ona z serwerem i nawiązuje połączenie
        //  za pomocą "Handshake'a". W naszym przypadku jest to odbiór wiadomości z sewera, wysłanie wiadomości
        //  i znowu odbiór wiadomości. Po wykonaniu tego "Handshake'a" otwierany Socket zaczyna wysyłać
        //  obraz z kamery, sensory oraz żądania w trybie live, oraz nasłuchuje odpowiedzi z serwera.
        mRun = true;
        try {
            InetAddress server_ip = InetAddress.getByName(SERVER_IP);
            Log.d("TCP", "Connecting to server...");
            try {
                //  "Handshake"
                socket_client = new Socket(server_ip, SERVER_PORT);
                in_msg_socket = new BufferedReader(new InputStreamReader(socket_client.getInputStream()));
                in_msg_socket.readLine();
                send_msg("Mobile socket");
                in_msg_socket.readLine();
                Log.d("TCP", "Camera socket connected!");
                ImageButton btn = (ImageButton)mActivityInstance.findViewById(R.id.imageButton);
                btn.setBackgroundColor(Color.GREEN);
                Button connectbtn = (Button)mActivityInstance.findViewById(R.id.connectbtn);
                connectbtn.setText(R.string.disconnect);

                //  Otworzenie MessageListenerThread, który nasłuchuje wiadomości z serwera
                MessageListenerThread msg_listener = new MessageListenerThread();
                Thread msg_thread = new Thread(msg_listener);
                msg_thread.start();

                while (mRun) {
                    //  Otwieranie SendDataThread, który wysyła dane do serwera
                    if (!socket_is_sending) {
                        SendDataThread send_cam = new SendDataThread();
                        send_cam.execute();
                    }


//                    if (error_count > 300) {
//                        Log.e("TCP", "Connection with server lost!");
//                        mActivityInstance.restart_connection();
//                    }
                    Thread.sleep(30);
                }
            } catch (Exception e) {
                Log.e("TCP", "Socket error: ", e);
                mActivityInstance.restart_connection();
            }
        } catch (Exception e) {
            Log.e("TCP", "Socket error: ", e);
            mActivityInstance.restart_connection();
        }
    }


    class MessageListenerThread implements Runnable{
        //  Klasa otwierana jako osobny wątek, ma za zadanie nasłuchiwać wiadomości z serwera.
        //  Działa tak długo, jak długo nawiązane jest połączenie z serwerem.
        public void run() {
            while (mRun) {
                try {

                    String server_message = in_msg_socket.readLine();

                    if (server_message.equals("calibrated"))
                       mActivityInstance.openDialog(4);
                    else if (server_message.equals("finished"))
                        mActivityInstance.openDialog(5);
                    else  if (server_message.equals("point"))
                        mActivityInstance.openDialog((2));
                    else  if (server_message.equals("Calibration error"))
                        mActivityInstance.openDialog((1));
                    else  if (server_message.equals("robot"))
                        mActivityInstance.openDialog((0));


                    Log.d("TCP", "Message from server: " + server_message);
                    Thread.sleep(10);
                }
                catch (Exception e) {
                    Log.e("TCP", "Message listener: " + e);
                    ImageButton btn = (ImageButton)mActivityInstance.findViewById(R.id.imageButton);
                    btn.setBackgroundColor(Color.RED);
                }
            }
        }
    }


    private class SendDataThread extends AsyncTask<Void, Void, Void> {
        //  Klasa otwierana wykonywana jako AsyncTask. Ma ona za zadanie wysyłać wszystkie dane do serwera.
        //  Działa tak długo, aż wyśle wszystkie wspomniane dane.
        protected Void doInBackground(Void... params) {
            //  Zamiana zapisanych wartości sensorów z Float na String
            String acc_x = Float.toString(mActivityInstance.sensor_values[0]);
            String acc_y = Float.toString(mActivityInstance.sensor_values[1]);
            String acc_z = Float.toString(mActivityInstance.sensor_values[2]);
            String gyr_x = Float.toString(mActivityInstance.sensor_values[3]);
            String gyr_y = Float.toString(mActivityInstance.sensor_values[4]);
            String gyr_z = Float.toString(mActivityInstance.sensor_values[5]);
            try {
                OutputStream os = socket_client.getOutputStream();
                //  Wysyłanie obecnej klatki z obrazu kamery w formie tablicy bitów.
                DataOutputStream dos = new DataOutputStream(os);
                socket_is_sending = true;
                dos.writeUTF("$%$#@@#$%$");
                dos.write(mActivityInstance.mPreview.mFrameBuffer.toByteArray());
                dos.writeUTF("$%$-@@-$%$");

                //  Wysyłanie informacji o współrzędnych kliknięcia na ekran, jeśli takie nastąpiło.
                if (mActivityInstance.click_values[0] != -1.0f) {
                    String click_x = Float.toString(mActivityInstance.click_values[0]);
                    String click_y = Float.toString(mActivityInstance.click_values[1]);
                    mActivityInstance.click_values[0] = -1.0f;
                    mActivityInstance.click_values[1] = -1.0f;
                    dos.writeUTF("$%$click" + click_x + "/" + click_y + "/$%$");
                }
                //  Wysyłanie do serwera żądania, które znajduje się na początku kolejki request_queue.
                //  Przykładem takiego żadania może być żądanie kalibracji, wysyłane do serwera po naciśnięciu przycisku.
                if (!request_queue.isEmpty()) {
                    String req = request_queue.remove();
                    dos.writeUTF("$%$req" + req + "$%$");
                }

                //  Wysyłanie danych z sensorów
                dos.writeUTF("$%$sens" + acc_x + "/" + acc_y + "/" + acc_z + "/" +
                        gyr_x + "/" + gyr_y + "/" + gyr_z + "/$%$");
                dos.flush();
                Thread.sleep(10);
                socket_is_sending = false;
            }
            catch (Exception e) {
                Log.e("TCP", "Ssocket error:", e);
            }
            return null;
        }
    }


    private void send_msg(final String msg) {
        //  Prosta metoda, służąca do wysyłania do serwera krótkich wiadomości
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    OutputStream os = socket_client.getOutputStream();
                    DataOutputStream dos = new DataOutputStream(os);
                    dos.writeUTF(msg);
                    dos.writeUTF("$%$");
                    dos.flush();
                }
                catch (Exception e) {
                    Log.e("TCP", "Socket error:", e);

                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }


    public void add_request(String request) {
        //  Metoda, która dodaje żądanie na koniec kolejki żądań request_queue
        //  Jest ona wywoływana z innych klas (np. po naciśnięciu przycisku Kalibracji)
        request_queue.add(request);
    }


    public void disconnect_message() {
        //  Metoda, która wysyła do serwera informację o zakończeniu połączenia.
        if (socket_client != null)
        {
            send_msg("Disconnecting");
        }
    }


    public void close_connection() {
        //  Metoda, która zamyka połączenie z serwerem, jeśli jest ono nawiązane.
        mRun = false;
        request_queue = null;
        try {
            socket_client.close();
        }
        catch (Exception e) {
            Log.e("TCP", "Empty socket");
        }
    }



}
