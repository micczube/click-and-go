﻿# Click and Go dokumentacja
#
## Spis plików
###### Serwer:
- gui_files
    - server_gui.ui - plik, w którym znajduje się UI serwera stworzone w QT Designerze
    - server_gui.py - plik wygenerowany z pliku server_gui.ui przez PyQt5
- images - folder z obrazami używanymi w programie
- temp - folder zawierający pliki z zapisaną konfiguracją programu
- GUI.py - główne okno GUI aplikacji
- TCP_server.py - zawiera klasę serwera oraz klasę obsługi połączenia z klientem
- determine_initial_robot_orientation.py - obliczenie orientacji startowej robota na podstawie jego początkowego ruchu
- image_processing.py - plik obsługujący detekcję obiektów, mapowanie, kalibrację
- kalman_filter.py - estymacja położenia przy użyciu filtru Kalmana (biblioteka pykalman)
- kalman_filter_own_impl.py - własna implementacja filtru Kalmana
- main_module.py - główny wątek programu, umożliwia komunikację między resztą wątków
- robot_control.py - klasa obsługująca sterowanie robotem
- utilities.py - funkcje użytkowe, wykorzystywane w innych plikach
- wyznaczanie_trasy.py - wyznaczenie trasy na podstawie obrazu. Korzysta z funkcji z pliku wyznaczanie_trasy_graf.py
- wyznaczanie_trasy_graf.py - funkcje umożliwiające skonstruowanie grafu i wyznaczenie trasy

###### Aplikacja:
- CameraView.java - klasa odpowiedzialna za obsługę kamery i udostępnianie obrazu z niej
- CreateDialog.java - klasa odpowiedzialna za tworzenie okna dialogowego
- MainActivity.java - główna klasa programu
- SettingsActivity.java - klasa aktywności ustawień w której możemy ustawić port oraz ip serwera
- StartActivity.java - klasa startowej aktywności, możemy zmienić adres ip oraz port 
- TcpClientThread.java - klasa obsługująca połączenie z serwerem

###### Robot:
- Arduino_code.ino - główny kod programu wgrywanego na Arduino
- IMU.ino - funkcje odpowiedzialne za obsługę czujnika IMU
- ComMotion_code.ino - główny kod programu wgrywanego na Motor Shielda
- Commands.ino - plik zawierający funkcję obsługującą komendy odbierane z Arduino
- EEPROM.ino - plik zapisujący i odczytujący pamięć EEPROM Motor Shielda
- I2C_Receive.ino - plik zawierający funkcję odbierającą dane z Arduino przez komunikację I2C
- I2C_Send.ino - plik zawierający funkcję wysyłającą dane do Arduino przez komunikację I2C
- IOpins.h - plik zawierający definicje pinów na Motor Shieldzie
- Motors.ino - plik zawierający funkcję obsługi silników oraz funkcje przerwań enkoderów
- PowerDown.ino - plik zawierający funkcję obsługującą niski poziom baterii
- Trigonometry.ino - plik zawierający funkcję wykonującą niezbędne obliczenia trygonometryczne
- TCP_robot_client.py - plik zawierający klasę obsługującą komunikację z serwerem i klasę obsługującą komunikację z Arduino przez Serial

#
## Przyjęte rozwiązania
####  Aplikacja na telefon
##### Skalowanie miejscia klikniecia na ekran
Dane z miejsca klikniecia na ekran są odczytowane przy pomocy funkcji onTouchEvent() a następnie zapisywane do odpowiedniej tablicy. 
Ponieważ zwracane wartości klikniecia na ekran nie są odpowiednio przeskalowane, to aby były dobrze zinterpretowane przez część odpowiedzialną za przetwarzanie obrazu,
zostały one przeskalowane w następujący sposób: 
```sh
float x = event.getX(pointerIndex);
float y = event.getY(pointerIndex);
click_values[0] = 1000 * x/width;
click_values[1] = 1000 * y/height;
```
##### Odczytywanie danych danych z sensorów
Dane z sensorów są odczytywane dzięki funkcji onSensorChanged(), a następnie rozrózniane w zależności od typu sernsora - akclereometr czy żyroskop, a następnie
zapisywany w odpowowiednie miejsca w tablicy  w sposób pokazany poniżej:
```sh
if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            sensor_values[0] = x;
            sensor_values[1] = y;
            sensor_values[2] = z;
        }
```

##### Usunięcie paska baterii i godziny
Aby aplikacja rozciągała się na cały ekran oraz żeby zapobiec przypadkowym kliknięciom na pasek z baterią i godziną, a co za tym idzie przesyłem niewłaściwych wartości jako parametry kliknięcia na ekran,
został on usunięty i nie pojawia się w momencie gdy kamera aplikacji jest włączona. 


#### Protokół połączenia przez TCP/IP
##### Handshake
Po przyłączeniu się do serwera, klient musi wykonać pewne czynności, zanim zaczną one komunikować się ze sobą. Ma to na celu uniemożliwić postronnym osobom przyłączenie się do serwera - stanowi to pewnego rodzaju hasło. Dodatkowo klient przysyła informacja identyfikacyjne, które przekazują serwerowi informację na temat tego, jaki rodzaj urządzenie podłączył się do niego.
Handshake przebiega w następujący sposób:
1. Serwer wysyła wiadomość "Welcome to server"
2. Klient musi się przedstawić wysyłając wiadomość "Robot socket"(w przypadku robota) lub "Mobil socket"(w przypadku telefonu)
3. Serwer potwierdza pomyślny przebieg identyfikacji wysyłając wiadomość "Message received!"

Jeśli czynności te przebiegną pomyślnie, to urządzenia przystępują do normalnej komunikacji. W przeciwnym wypadku połączenie jest zakańczane.
Przykład obsługi Handshake'a ze strony aplikacji na telefon
```sh
in_msg_socket.readLine();
send_msg("Mobile socket");
in_msg_socket.readLine();
```

##### Separator wiadomości
Przesył wiadomości za pomocą protokołu TCP/IP wiąże się z pewnymi komplikacjami. Wiadomości, wysłane przez klienta jako osobne, mogą pojawić się na serwerze jako jedna wiadomość. Z tego powodu postanowiliśmy rozdzielać poszczególne wiadomości separatorem, zdecydowaliśmy się aby był to ciąg znaków "$%$". Wiadomości, które mają być traktowane jako osobne są poprzedzone i zakończone tym separatore, później na serwerze są rozdzielane. Ponieważ niektóre wiadomości nie mogą być dekodowane, proces separacji musi być wykonywany na danych otrzymanych jako tablica bajtów.
```sh
received_data = []
len1 = 0
len2 = 0
for i in range(2, len(data_in)):
    if data_in[i - 2] == 36 and data_in[i - 1] == 37 and data_in[i] == 36:
        data_temp = data_in[len1: len2]
        received_data.append(data_temp)
        len1 = i + 1
    else:
        len2 = i - 1
received_data.append(data_in[len1: len(data_in)])
```

##### Przesył obrazu z kamery
Ze względu na duży obrazu z kamery zamienionego na tablicę bajtów może się zdarzyć, że różne części zdjęcia przyjdą jako osobne wiadomości. Z tego powodu należy przekazać serwerowi informację o tym, którą część otrzymanych danych ma traktować jako jedno zdjęcie. W tym celu przesyłamy ciągi znaków informujące o początku oraz końcu przesyłu obrazu. Są to odpowiednio "#@@#" i "-@@-". Wszystkie dane odebrane pomiędzy tymi wiadomościami są traktowane jako część jednego zdjęcia i zapisywane do jednej tablicy.
Po otrzymaniu wiadomości "-@@-", która informuje o końcu przesyłu obrazu, zdjęcie jest zapisywane.
```sh
self.image_data = b''
```
```sh
if msg[0:4] == '#@@#':
    self.getting_image = True
```
```sh
elif self.getting_image is True:
    self.image_data += data
```
```sh
if self.getting_image is True and (msg[0:4] == '-@@-'):
    if len(self.image_data) > 1:
        self.getting_image = False
        f = open('images/received.jpg', 'wb')
        f.write(self.image_data)
        f.close()
        self.image_data = b''
```

##### Przesył wiadomości
Przesyłając wiadomość na serwer klient musi poprawnie określić jej typ. W tym celu stosujemy odpowiednie przedrostki wstawiane przed treścią wiadomości. Przykładowo wysyłając wiadomość o klinięciu na telefonie, musimy ją poprzedzić ciągiem znaków "click".
```sh
if msg[0:5] == 'click':
```
Jeśli przesłana zostanie wiadomość bez odpowiedniego przedrostka, to nie zostanie ona odpowiednio obsłużona na serwerze.
| Przedrostek | Zawartość wiadomości |
| ------ | ------ |
| #@@# | Rozpoczęcie transferu obrazu |
| -@@- | Zakończenie transferu obrazu |
| click | Współrzędne kliknięcia na ekran |
| sens | Dane z sensorów z telefonu |
| IMU | Dane z IMU z robota |
| Enk | Dane z enkoderów z robota |
| Disconnecting | Informacja o zakończeniu połączenia |
| req | Żądanie z telefonu |
| msg | Wiadomość do wyświetlenie |

Dodatkowo, wiadomości o przedrostku "req"(żądania z telefonu), są również przetwarzane w ten sposób.
| Przedrostek | Typ żądania |
| ------ | ------ |
| calibration | Żądanie rozpoczęcia kalibracji |

##### Przesył danych liczbowych
Często zdarza się, że chcemy przesłać dane liczbowe zapisywane w tablicy. Aby odpowiednio je rozdzielać i zapisywać zastosowaliśmy separator '/'. W tym przypadku możemy rozdzielać dane już po dekodowaniu ich.
```sh
click_data = msg[5:].split('/')
```

##### Przesył danych z serwera na robota
Tak samo jak w przypadku przesyłu danych z klienta na serwer, wiadomości przesyłane z serwera na robota muszą posiadać odpowiedni przedrostek informujący o ich zawartości.
| Przedrostek | Zawartość wiadomości |
| ------ | ------ |
| RESET | Informacja o konieczności przeprowadzenia resetu robota |
| STOP | Żądanie zatrzymania robota |
| VEL | Prędkość, z jaką robot ma jechać |
| ANGLE | Kąt |
| ROT | Rotacja |

##### Optymalizacja socketów
W celu optymalizacji działania programu zdecydowaliśmy się na użycie nieblokujących socketów.
```sh
(connection, (ip, port)) = self.server.accept()
connection.setblocking(False)
```
Dzięki temu oczekujemy od socketa na informację o nowych danych, zamiast ciągle sprawdzać to w pętli:
```sh
ready_to_read, _, _ = select.select((self.connection,), (), (), 60)
```
Dodatkowo, w celu udogodnienia życia użytkownikowi, daliśmy serwerowi możliwość wykorzystywania zajętych juz portów.
```sh
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
```

##### Wykrywanie zerwanych połączeń
Ze strony serwera zerwane połączenia możemy wykrywać kilka sposobów:
1. Błąd przy wysyłaniu wiadomości 
    ```sh
    try:
        self.connection.sendall(data_out)
    except socket.error:
        self.close_connection('error')
    ```
2. Błąd przy odbiorze wiadomości
    ```sh
    try:
        data_t = self.connection.recv(25000)
    except socket.error:
        self.close_connection('error')
    ```
3. Timeout połączenia - nieotrzymanie nowej wiadomości w określonym przedziale czasu
    ```sh
    ready_to_read, _, _ = select.select((self.connection,), (), (), 60)
    if len(ready_to_read) == 0:
        self.close_connection('error')
    ```
4. Otrzymanie pustej wiadomości(o długości 0)
    ```sh
    if len(data_in) == 0:
        self.close_connection('error')
    ```

##### Sprawdzanie poprawności adresu IP i portu serwera
Może się zdarzyć, że użytkownik wpisze dane serwera, które uniemożliwiają jego poprawne działanie. W takim przypadku należy to wykryć i możliwie dokładnie poinformować użytkownika, co zrobił źle. Rozwiązujemy to w następujący sposób
1. Sprawdzenie poprawności składni adresu IP 
    ```sh
    socket.inet_aton(self.ui.ip_textbox.text())
    ```
2. Sprawdzenie, czy port jest wartością integer oraz czy mieści się w przedziale
    ```sh
    int(self.ui.port_textbox.text())
    if 0 <= int(self.ui.port_textbox.text()) <= 65535:
    ```
3. Sprawdzenie, czy dany adres IP jest poprawny dla komputera
    ```sh
    server_temp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_temp.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_temp.bind((self.ui.ip_textbox.text(), int(self.ui.port_textbox.text())))
    ```
4. (Opcjonalne) Sprawdzenie, czy port serwera jest niezajęty
    ```sh
    conn_result = server_temp.connect_ex((self.ui.ip_textbox.text(), int(self.ui.port_textbox.text())))
    if conn_result != 0:
        port_occupied = True
    ```
#### GUI Aplikacji

##### Przesyłanie danych pomiędzy aktywnościami
Do przesyłania danych pomiędzy aktywnościami wykorzystana zostały obiekty klasy Bundle (odbieranie) oraz Intent (załączanie)
1. Odbieranie danych
    ```sh
    Bundle bb;
    bb = getIntent().getExtras();
    assert bb != null;
        
    if (!bb.getString("port").isEmpty()) {
        STRING_PORT = bb.getString("port");
        SERVER_PORT = Integer.parseInt(STRING_PORT);
    ```
2. Wysyłanie danych
    ```sh
    Intent intent = new Intent(this, MainActivity.class);
    intent.putExtra ( "ip", ip.getText().toString() );
    intent.putExtra ( "port", port.getText().toString() );
    startActivity(intent);
    ```
Innym rozwiązaniem do przesłania i odebrania danych jest obiekt klasy SharedPreference
1. Odbieranie danych
    ```sh
    SharedPreferences settings = getSharedPreferences("CaG", Context.MODE_PRIVATE);
    SERVER_IP = settings.getString("ip_address", "");
    STRING_PORT = settings.getString("port_number", "");
    ```
2. Wysyłanie danych
    ```sh
    SharedPreferences settings = getSharedPreferences("CaG", Context.MODE_PRIVATE);
    EditText ipEdit = (EditText)findViewById(R.id.ipEditText);
    EditText portEdit = (EditText)findViewById(R.id.portEditText);
    SharedPreferences.Editor editor = settings.edit();
    editor.putString("ip_address", ipEdit.getText().toString());
    editor.putString("port_number", portEdit.getText().toString());
    editor.apply();
    ```
##### Tworzenie okien dialogowych
Okna dialogowe są tworzone w opraciu o klasę AlertDialog. Przy pomocy obiektu klasy Intent i Bundle przesyłamy do okna informację jaki wyświetlić komunikat. Okno dialogowe posiada swój wygląd stworzony w pliku dialog_layout.xml.

    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    LayoutInflater inflater = requireActivity().getLayoutInflater();
    View promptView = inflater.inflate(R.layout.dialog_layout, null);
    builder.setView(promptView);
    int w = getArguments().getInt("which_one");
##### Ekran startowy oraz ustawienia
Obie aktywności mają możliwość wprowadzenia adresu IP oraz portu do którego aplikacja podłącza sie, wykorzystane zostały pola tekstowe, przesyłają je tak jak zostało opisane to wyżej. Ekran startowy jest połączony z ustawieniami, tj. ustawiając adres IP na aktywności startowej zostanie on przekazany do ustawień.


#### Wątki
##### Wykorzystanie Eventów z modułu threading
W celu optymalizacji działania aplikacji postanowiliśmy wykorzystać "zdarzenia" oferowane przez moduł threading. Pozwala nam to sterować wątkami z zewnątrz, przekazując im informację o nowych danych. Dzięki temu wątki nie będą się wykonywać, jeśli nie otrzymają nowych danych. Wyjątkami są tu wątki, które muszą działać niezależnie od tego, czy otrzymały nowe dane, czy nie.
Wykorzystanie Eventów odbywa się w nastepujący sposób:
1. Tworzymy obiekt klasy threading.Event()
    ```sh
    new_information_event = threading.Event()
    ```
2. Dany wątek oczekuje na ustawienie flagi Eventu
    ```sh
    new_information_event.wait()
    ```
3. Ustawiamy flagę Eventu
    ```sh
    new_information_event.set()
    ```
4. Resetujemy flagę Eventu(inaczej wątek będzie się wykonywał w pętli)
    ```sh
    new_information_event.clear()
    ```


##### Odświeżanie GUI
Zgodnie z dokumentacją biblioteki PyQt5, wartości kontrolek GUI nie mogą być zmieniane z zewnętrznych wątków. Może to prowadzić do nieprawidłowego działania programu, lub w ekstremalnych sytuacjach nawet do jego crashu. Z tego powodu kontrolki GUI muszą być odświeżane przez ten samą wątek, który stworzył GUI. W tym celu wykożystaliśmy obiekt QTimer(), który w stałych interwałach czasu wywołuje funkcję odświeżającą.
```sh
timer = QTimer()
timer.singleShot(30, self.update_gui)
```
##### Zabezpieczenie przed wpisaniem innych znaków niż cyfry w label z numerem portu
Aby zapewnić poprawne wpisanie numeru portu przez użytkownika użytko funkcji QIntValidator() z biblioteki PyQt5. Działa to w ten sposób, że jeśli użytkownik spróbuje wpisac literę, bądź inny znak ktory nie jest cyfrą,
nie zostanie on wpisany w label z adresem portu. 
```sh
only_int_val = QIntValidator()
self.ui.port_textbox.setValidator(only_int_val)
```
##### Informacje o połączeniu
Aby zapewnić użytkownikowi pewność, że serwer jest połączony z poszczególnymi podmiotami oraz, że połączenie nie zostało zerwane, ustawiane są odpowiednie komuniktaty w GUI aplikacji. Jeśli serwer ma nawiązął połącznie z robotem lub aplikacją mobilną ustawia flagę     

##### Box z powiadomieniami
W celu powiadomienia użytkownika o tym co się aktualnie dzieje w aplikacji, został dodany box z powiadomieniami, informującymi
między innymi czy podany adres IP jest poprawny, czy udało się uruchomić serwer oraz o nawiązanych połączeniach i ich rodzajach. 
Ponieważ może przychodzić kilka powiadomień w krótkich odstępach czasu, co mogło być sprawić, że w jednym momencie kilka wątków chce
zaktualizowac tablicę powiadomień, utworzono kolejkę, która w funkcji odswieżąjącej gui po koleji dodaje nowe informacje do tablicy
powiadomień.

```sh
self.ui.info_textbox.setText('')
    for msg in self.info_textbox_text:
        self.ui.info_textbox.append(msg)
```

##### Obraz z kamery telefonu oraz obraz przetworzony
Obrazy z kamery telefonu obraz przetworzony ukazujący przeszkody wykryte przez robota oraz drogę robota wyznaczoną przez algorytm 
optymalizacji trasy są dodawane do GUI za pośrednictwem sceny graficznej i aktualizowane jednocześnie ze wszystkimi elementami
interfejsu graficznego. 
```sh
 scene = QGraphicsScene()
        item = QGraphicsPixmapItem()
        item.setPixmap(QPixmap(self.phone_image).scaled(self.ui.phone_img_view.width() - 10,
                                                        self.ui.phone_img_view.height(), Qt.KeepAspectRatio))
```

####  Instalator
WINDOWS
Aby zainstalować aplikacje serwera na komputerze, należy odpalić plik ClickAndGo installer i postępować zgodnie z instukcją na ekranie. Po instalacji, wystarczy odpalić skrót który został utworzony na pulpicie (lub jeśli nie należy wejść w Program Files (x86)\ClickAndGo i uruchomić
plik wykonawczy o nazwie main_module.exe),następnie po chwili uruchomi się aplikacja gotowa do użytku. 
LINUX
Należy uruchomić instalator lub wypakować plik zip, następnie postępować zgodnie z instukcją pliku README, który znajduje się w folderze, gdzie zainstalowano aplikację.


#### Budowa robota
##### Arduino Uno Rev3
Arduino jest u nas głównym elementem budowy robota. Z jednej strony komunikuje się ono z Raspberry Pi a z drugiej steruje Motor Shieldem, który odpowiada za kontrolę silników.
Arduino jest podłączone do Raspberry Pi przez USB Serial Port i z tego połączenia też czerpie zasilanie. Komunikacja z Raspberry Pi przebiega w taki sposób, że gdy są dostępne dane na Serial Porcie jest on czytany do znaku końca linii (jedna komenda sterująca w danym momencie). Jak tylko zostanie odczytana zmienna o typie String z Serial Porta wysyłane są nań dane z czujników odseparowane ciągiem znaków "$%$". W ten sposób Raspberry Pi również czyta dane tylko do znaku końca linii i odpowiednio je rozdziela.
```sh
response = Serial.readStringUntil('\n');

Serial.print(encoders_data);
Serial.print("$%$");
Serial.println(IMU_data);
```
Na Arduino nałożony jest Motor Shield sterujący silnikami, z którym Arduino komunikuje się przez magistralę szeregową I^2^C.
Arduino wysyła do Motor Shielda dane sterujące, które pozyskuje po odpowiednim rozdzieleniu Stringa otrzymanego przez Serial Port z Raspberry Pi. Dane wysyłane są na magistralę szeregową w następujący sposób:
```sh
Wire.beginTransmission(30);             // 30 to adres pierwszego procesora na Motor Shieldzie
Wire.write(3);                          // Jako pierwsza dana wysyłany jest numer komendy
Wire.write(highByte(velocity));         // Zmienne typu Integer muszą być wysyłane jako 2 bajty
Wire.write( lowByte(velocity));
Wire.write(highByte(angle));
Wire.write( lowByte(angle));
Wire.write(highByte(rotation));
Wire.write( lowByte(rotation));
Wire.endTransmission();
```
Po wysłaniu danych sterujących Arduino zbiera dane z czujników i zapisuje je do zmiennych globalnych, które w każdej pętli po otrzymaniu komendy od Raspberry Pi są też na niego wysyłane. Dane enkoderów z Motor Shielda zbierane są w następujący sposób:
```sh
if (Wire.requestFrom(30, 4) == 0){
  ;
}
else{
  zliczenia_enkoderow[0] = Wire.read();
  zliczenia_enkoderow[0] <<= 8;
  zliczenia_enkoderow[0] |= Wire.read();
  zliczenia_enkoderow[1] = Wire.read();
  zliczenia_enkoderow[1] <<= 8;
  zliczenia_enkoderow[1] |= Wire.read();
}
```
Przy czym prośba o dane z Motor Shielda wysyłana jest dwukrotnie - osobno na każdy z procesorów.
Następnie Arduino sczytuje dane z IMU i zapisuje je do odpowiedniego Stringa.
##### ComMotion Motor Shield
Płytka Motor Shield nałożona jest na Arduino, ale zasilana jest z osobnego źródła - koszyczka z sześcioma akumulatorkami Ni-MH AA, każdy o napięciu 1,2 V. Motor shield wykonuje wszystkie potrzebne obliczenia do sterowania silnikami, ustala odpowiednie wartości na pinach oraz sczytuje dane z enkoderów.
##### Raspberry Pi 2B
Raspberry Pi zasilane jest z powerbanka na robocie. Odpowiedzialne jest ono za komunikację WiFi z serwerem na laptopie po protokole TCP/IP napisanym w pythonie oraz za komunikację z Arduino przez Serial Port.
Przy wykorzystaniu biblioteki serial-py tworzony jest obiekt obsługujący Serial Port Raspberry, następnie w pętli najpierw wypisywany jest na ten port String sterujący, a zaraz potem odczytywany jest String wysłany przez Arduino zawierający dane z czujników.
```sh
arduino_serial = serial.Serial(usb, baud, timeout=0.3)
arduino_serial.write(self.message.encode('utf-8'))
self.arduino_serial.readline().decode('utf-8').rstrip()
```
Raspberry wysyła 3 zmienne sterujące w stringu w postaci "steering:velocity/angle*rotation&\n". Zmienne zostały oddzielone unikatowymi znakami, aby na Arduino jak najłatwiej pozyskać trzy wartości typu integer, znajdując indeksy odpowiednich znaków.
Raspberry po przeczytaniu danych z seriala rozbija stringa na dwa osobne stringi "dane_z_enkoderow" i "dane_z_imu" po czy przesyła je na serwer. Raspberry Pi 2B nie jest standardowo wyposażone w moduł WiFi, dlatego korzystamy z modułu wpinanego w USB. 


#### Przetwarzanie obrazu
##### Detekcja obiektów
Wykrywanie robota oraz przeszkód realizowane jest jako progowanie obrazu na podstawie zakresów wartości HSV. Tak sprogowany obraz poddawany jest różnym operacjom morfologicznym, które poprawiają jakość detekcji. Przykład:
```sh
kernel = np.ones((8, 8), np.uint8)
orange_threshed = cv2.morphologyEx(orange_threshed, cv2.MORPH_CLOSE, kernel)
```
Zakresy HSV ustalane są na podstawie wcześniejszej kalibracji. Aby dodatkowo poprawić odporność algorytmu na niedoskonałości, klatka z wyrysowanymi obiektami (zwracana przez funkcję find_objects) jest poddawana operacji logicznej AND z klatką poprzednią.
W ten sposób, gdy obiekt "zniknie" na czas jednej klatki, nie będzie to widoczne w programie:
```sh
if old_frame is not None:
    frame_2D = cv2.bitwise_and(frame_detected, old_frame)
else:
    frame_2D = frame_detected
old_frame = frame_detected
```
##### Kalibracja początkowa
W procesie kalibracji bierze udział jedna klatka obrazu. Po rozmyciu i odpowiednim przeskalowaniu znajdowany jest na niej kalibrator w postaci białej kartki papieru formatu A4.
Dane o rogach kartki, które są odpowiednio wyliczane, pozwalają nam na wykonanie przekształcenia perspektywicznego - czyli efektywnego zmapowania widoku na "lot ptaka". Ponadto, posiadamy wiedzę, że kartka jest idealnie biała.
Pozwala to na wyliczenie poprawki kolorów (r0, g0, b0), która musi być uwzględniona przy wyznaczaniu zakresów HSV progowania do detekcji obiektów - pozwala to na znaczną poprawę działania algorytmu w różnych warunkach świetlnych.
```sh
orange_rgb[0] = np.min([255, orange_rgb[0] + r0])
orange_rgb[1] = np.min([255, orange_rgb[1] + g0])
orange_rgb[2] = np.min([255, orange_rgb[2] + b0])
orange = np.uint8([[[orange_rgb[0], orange_rgb[1], orange_rgb[2]]]])
orange_hsv = cv2.cvtColor(orange, cv2.COLOR_RGB2HSV)
h0, s0, v0 = orange_hsv[0, 0, 0], orange_hsv[0, 0, 1], orange_hsv[0, 0, 2]
```
Tak wyznaczone wartości HSV z indeksem "0" to środek zakresu hsv, do którego potem odpowiednio dodajemy i odejmujemy wartości marginesów. 
##### Początkowa orientacja robota
Robot na początku powinien przejechać określony odcinek w kierunku swojej lokalnej osi X. Jego pozycja na obrazie jest zapamiętywana, a później przybliżana prostą, co umożliwia wyznaczenie jego początkowej orientacji.  
Dopasowanie prostej odbywa się metodą najmniejszych kwadratów.
```python
def fit_line_and_get_y_values(x_arr, y_arr):
    """
    Dopasowanie linii metodą najmniejszych kwadratow na podstawie otrzymanych wspolrzednych
    :param x_arr: lista wspolrzednych x robota zebrana z poszczegolnych klatek
    :param y_arr: lista wspolrzednych y robota zebrana z poszczegolnych klatek
    :return: lista skladajaca sie z wartosci linii dopasowanej do otrzymanych wspolrzednych (y = mx + c)
    """
    x_arr = np.asarray(x_arr)
    A = np.vstack([x_arr, np.ones(len(x_arr))]).T
    m, c = np.linalg.lstsq(A, y_arr, rcond=None)[0]
    fitted_y = m * x_arr + c
    return fitted_y
```

##### Mapowanie kliknięcia
Mapowanie kliknięcia odbywa się na tej samej zasadzie, co mapowanie perspektywy. Najpierw zaznaczam na pustej masce miejsce kliknięcia.
```sh
unwarped_mask = np.ones(img.shape[:2], dtype="uint8") * 255
unwarped_mask = cv2.circle(unwarped_mask, (x, y), 8, color=2, thickness=-1)
```
Następnie, przy pomocy przekształcenia perspektywicznego przez macierz wyznaczoną przy kalibracji początkowej mapuję obraz na "lot ptaka".
Na tak zmapowanym obrazie szukam narysowanego kliknięcia, z którego łatwo odczytać współrzędne zmapowane:
```sh
warped_mask = cv2.warpPerspective(unwarped_mask, matrix, (600, 400))
warped_mask = cv2.bitwise_not(warped_mask)
contours, _ = cv2.findContours(warped_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
```
Najmniejszy ze znalezionych konturów to nasze kliknięcie (na masce zaznaczony jest również bok obrazu spowodowany przekształceniem perspektywy).
```sh
try:
    moments = cv2.moments(contours[smallest_index])  # liczenie momentow konturu (dane na temat ksztaltu)
    if moments["m00"] != 0 and smallest < 1000:  # unikanie dzielenia przez zero
        new_x = int(moments["m10"] / moments["m00"])  # z momentów mamy środek konturu
        new_y = int(moments["m01"] / moments["m00"])
except IndexError:
    print("Index error przy wyznaczaniu zmapowanego klikniecia")     
```

#### Sterowanie robotem

##### Układ współrzędnych
Położenie robota jest przechowywane w układzie współrzędnych o początku w miejscu, w którym robot zakończył ruch w celu wyznaczenia orientacji początkowej i o orientacji zgodnej z orientacją, którą robot miał w tym punkcie.

Przekształcenie z współrzędnych obrazu odbywa się poprzez przemnożenie wektora położenia na obrazie przez macierz 3x3 zawierającą rotację, translację oraz odbicie lustrzane osi Y i opierającą się na początkowej orientacji robota oraz jego przesunięciu w osiach X oraz Y.
```
transform_matrix = np.array([[cos(ang), -sin(ang), -h * cos(ang) + k * sin(ang)],
                                 [-sin(ang), -cos(ang), h * sin(ang) + k * cos(ang)],
                                 [0, 0, 1]])
```

##### Wyznaczanie trasy

Trasa jest wyznaczana w oparciu o sam obraz. Informacja z niego musi być najpierw przekształcona do postaci grafu, do stworzenia którego użyta jest biblioteka networkx.

Graf jest tworzony przez podzielenie obrazu na siatkę kwadratów o określonej długości boków i przyporządkowaniu każdemu z pól wierzchołka. Wierzchołki odpowiadające przeszkodom lub znajdujące się zbyt blisko nich są usuwane.

Najpierw dodawane są wierzchołki:
```
G_1 = grid_2d_graph(graph_size_x, graph_size_y)
G.add_nodes_from(G_1.nodes())
```
Następnie krawędzie:
```
G.add_edges_from([
                     ((x, y), (x + 1, y))
                     for x in range(graph_size_x - 1)
                     for y in range(graph_size_y)
                 ] + [
                     ((x, y), (x, y + 1))
                     for x in range(graph_size_x)
                     for y in range(graph_size_y - 1)
                 ], weight=1.0)

G.add_edges_from([
                     ((x, y), (x + 1, y + 1))
                     for x in range(graph_size_x - 1)
                     for y in range(graph_size_y - 1)
                 ] + [
                     ((x + 1, y), (x, y + 1))
                     for x in range(graph_size_x - 1)
                     for y in range(graph_size_y - 1)
                 ], weight=1.4)

```
A później usuwane są wierzchołki, które odpowiadają polom z przeszkodami (uznanie pola za przeszkodę ma miejsce, gdy jest w tym polu co najmniej jeden piksel zerowy czyli odpowiadający kolorem przeszkodzie):

```
nodes_to_remove = [(x, y) for x in range(graph_size_x)
                   for y in range(graph_size_y)
                   if np.count_nonzero(img[y*cell_size_y:(y+1)*cell_size_y, x*cell_size_x:(x+1)*cell_size_x] == 0) > 0]
```

Na koniec usuwane są wierzchołki będące blisko przeszkód:
```
for _ in range(margin):
        left_n = [(x[0]-1, x[1]) for x in nodes_to_remove if (x[0]-1, x[1]) not in nodes_to_remove]
        right_n = [(x[0]+1, x[1]) for x in nodes_to_remove if (x[0]+1, x[1]) not in nodes_to_remove]
        up_n = [(x[0], x[1]-1) for x in nodes_to_remove if (x[0], x[1]-1) not in nodes_to_remove]
        down_n = [(x[0], x[1]+1) for x in nodes_to_remove if (x[0], x[1]+1) not in nodes_to_remove]
        nodes_to_remove.extend(left_n)
        nodes_to_remove.extend(right_n)
        nodes_to_remove.extend(up_n)
        nodes_to_remove.extend(down_n)

G.remove_nodes_from(nodes_to_remove)

```

Ścieżka znajdywana jest przy pomocy algorytmu A*, z heurystyką będącą odległością euklidesową:
```
distance = lambda pos_1, pos_2: ((pos_2[0] - pos_1[0]) ** 2 + (pos_2[1] - pos_1[1]) ** 2) ** 0.5

path = nx.astar_path(G, start_node, end_node, heuristic=distance)
```

##### Filtr Kalmana

Filtr Kalmana miał w założeniu pracować w oparciu o dane z obrazu - pomiar położenia, enkoderów - pomiar prędkości oraz z IMU - pomiar przyspieszenia oraz prędkości obrotowej.

Wektor stanu ma więc postać:
```
"""
X = [
      x - położenie w osi X
      y - położenie w osi Y
      th - orientacja
      x* - prędkość w osi X
      y* - prędkość w osi Y
      th*
      x**
      y**
    ]
"""
```

Macierz tranzycji stanu odpowiadająca za jego estymację na podstawie poprzedniego stanu (krok czasowy - dT):
```
A = [[1, 0, 0, dT,  0,  0, (dT**2)/2,         0],
     [0, 1, 0,  0, dT,  0,         0, (dT**2)/2],
     [0, 0, 1,  0,  0, dT,         0,         0],
     [0, 0, 0,  1,  0,  0,        dT,         0],
     [0, 0, 0,  0,  1,  0,         0,        dT],
     [0, 0, 0,  0,  0,  0,         0,         0],
     [0, 0, 0,  0,  0,  0,         0,         0],
     [0, 0, 0,  0,  0,  0,         0,         0]]
```

Macierz H określa które zmienne stanu są mierzone:
```
H = [[1, 0, 0, 0, 0, 0, 0, 0],
     [0, 1, 0, 0, 0, 0, 0, 0],
     [0, 0, 0, 1, 0, 0, 0, 0],
     [0, 0, 0, 0, 1, 0, 0, 0],
     [0, 0, 0, 0, 0, 1, 0, 0],
     [0, 0, 0, 0, 0, 0, 1, 0],
     [0, 0, 0, 0, 0, 0, 0, 1]]
```
Macierze kowariancji Q i R określają błąd kolejno estymacji i pomiaru, jednak ich wartości nie zostały dostosowane ze względu na brak testów.

Za aktualną wartość obserwacji są uznawane zmienne globalne, przechowujące ostatnią wartość pomiaru:
```
[self.robot_image_position[0], self.robot_image_position[1], self.enkoders[0], self.enkoders[1], self.IMU[0], self.IMU[1], self.IMU[2]]
```

##### Algorytm sterowania
Algorytm sterowania robotem opiera się na wyznaczonej trasie. Analizowane jest obecne położenie robota oraz następny węzęł, do którego ma udać się robot. Algorytm wyznaczania trasy wykonywany jest dopóki lista *path* zawiera dwa lub więcej wierzchołków (w przeciwnej sytuacji oznacza to osiągnięcie punktu docelowego).  

Aktualne współrzędne są transformowane na nowy układ współrzędnych.
```python
transformed_robot_coor = transform_coor(self.x_start, self.y_start, self.angle,
                                        robot_pos_pix)
```

Współrzędne węzła z trasy są zamieniane na współrzędne na obrazie a następnie przekształcane do nowego układu współrzędnych.
```python
next_node_pix_x = (path[1][0] * 20) + 10
next_node_pix_y = (path[1][1] * 20) + 10
next_node_pix = (next_node_pix_x, next_node_pix_y)
transformed_next_node_pix = transform_coor(self.x_start, self.y_start, self.angle,
                                            next_node_pix)
```
Następnie wyznaczana jest różnica między uzyskanymi współrzędnymi.
```python
xs, ys = transformed_robot_coor[0], transformed_robot_coor[1]
xe, ye = transformed_next_node_pix[0], transformed_next_node_pix[1]
dy = ye - ys
dx = xe - xs
```

Na podstawie wartości *dx* oraz *dy* wyznaczany jest oczekiwany kąt kierunku jazdy robota. Kąt jest skierowany przeciwnie do ruchu wskazówek zegara. W celu poprawnej komunikacji z serwerem wartość kąta jest rzutowana na wartość całkowitą. Prędkość jazdy ustalana jest przez użytkownika.
```python
robot_vel = self.MAIN_THREAD.STATE.max_robot_velocity
robot_ang = int(0 - np.degrees(np.arctan2(dy, dx)) % 360.0)
```




